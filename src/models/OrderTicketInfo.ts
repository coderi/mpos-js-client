import { Type } from "class-transformer";

import { IsDefined, IsString, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import TicketLocation from "./TicketLocation";

export default class OrderTicketInfo extends BaseModel {
    @IsString()
    Id          : string;

    @IsDefined()
    @IsString()
    Name        : string;

    @ValidateNested()
    @Type(() => TicketLocation)
    Location    : TicketLocation;
}
