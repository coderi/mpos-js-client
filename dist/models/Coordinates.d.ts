import BaseModel from "./BaseModel";
export default class Coordinates extends BaseModel {
    X: number;
    Y: number;
}
