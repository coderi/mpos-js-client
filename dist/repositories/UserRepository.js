"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var class_transformer_1 = require("class-transformer");
var BaseRepository_1 = require("./BaseRepository");
var User_1 = require("../models/User");
var UserRepository = (function (_super) {
    __extends(UserRepository, _super);
    function UserRepository() {
        return _super.call(this, "/Users", User_1.default) || this;
    }
    UserRepository.prototype.create = function (model, onSuccess, onError) {
        var data = class_transformer_1.classToPlain(model);
        this.send("POST", "", data, function (data, response) {
            var user = BaseRepository_1.dataToModel(User_1.default, data.User);
            onSuccess(user, response);
        }, onError);
    };
    UserRepository.prototype.update = function (id, model, onSuccess, onError) {
        var data = class_transformer_1.classToPlain(model);
        this.sendToId(id, "PUT", "", data, function (data, response) {
            var user = BaseRepository_1.dataToModel(User_1.default, data.User);
            onSuccess(user, response);
        }, onError);
    };
    return UserRepository;
}(BaseRepository_1.RDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = UserRepository;
//# sourceMappingURL=UserRepository.js.map