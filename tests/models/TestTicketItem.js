var expect = require("chai").expect;

var mPosLib = require("../../dist/index.js");

var TicketItem = mPosLib.TicketItem;
var Money = mPosLib.Money;
var Quantity = mPosLib.Quantity;
var TicketItemPlu = mPosLib.TicketItemPlu;

describe("TicketItem", function() {
    describe("instantiated ticket item", function() {
        var ticketItem = new TicketItem();

        it("should have discount rate of type number", function() {
            expect(ticketItem.DiscountRate).to.be.an('number');
        });
        it("should have discount rate set to 0", function() {
            expect(ticketItem.DiscountRate).to.equal(0);
        });
    });

    describe("Ticket item with default values", function() {
        var currencyLabel = "EUR";

        var ticketItem = new TicketItem();
        ticketItem.DiscountRate = 0;
        
        ticketItem.UnitPriceBrutto = new Money();
        ticketItem.UnitPriceBrutto.Amount = 0;
        ticketItem.UnitPriceBrutto.CurrencyLabel = currencyLabel;

        ticketItem.Quantity = new Quantity();
        ticketItem.Quantity.Amount = 0;
        ticketItem.Quantity.Unit = "ks";
        
        ticketItem.Plu = new TicketItemPlu();
        ticketItem.Plu.IsDiscountAllowed = true;

        var normalizedDiscountRate = ticketItem.getNormalizedDiscountRate();
        var nettoUnitPrice = ticketItem.getNettoUnitPrice();
        var totalBruttoPrice = ticketItem.getTotalBruttoPrice();
        var totalNettoPrice = ticketItem.getTotalNettoPrice();
        var discountAmount = ticketItem.getDiscountAmount();

        it("should have normalized discount rate of type number", function() {
            expect(normalizedDiscountRate).to.be.an('number');
        });
        it("should have normalized discount rate set to 0", function() {
            expect(normalizedDiscountRate).to.equal(0);
        });
        it("should have netto unit price of type object", function() {
            expect(nettoUnitPrice).to.be.an('object');
        });
        it("should have netto unit price amount set to 0", function() {
            expect(nettoUnitPrice.Amount).to.equal(0);
        });
        it("should have netto unit price currency label set correctly", function() {
            expect(nettoUnitPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have total brutto price of type object", function() {
            expect(totalBruttoPrice).to.be.an('object');
        });
        it("should have total brutto price amount set to 0", function() {
            expect(totalBruttoPrice.Amount).to.equal(0);
        });
        it("should have total brutto price currency label set correctly", function() {
            expect(totalBruttoPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have total netto price of type object", function() {
            expect(totalNettoPrice).to.be.an('object');
        });
        it("should have total netto price amount set to 0", function() {
            expect(totalNettoPrice.Amount).to.equal(0);
        });
        it("should have total netto price currency label set correctly", function() {
            expect(totalNettoPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have discount amount of type object", function() {
            expect(discountAmount).to.be.an('object');
        });
        it("should have discount amount amount set to 0", function() {
            expect(discountAmount.Amount).to.equal(0);
        });
        it("should have discount amount currency label set correctly", function() {
            expect(discountAmount.CurrencyLabel).to.equal(currencyLabel);
        });
    });

    describe("Discounted ticket item with small numeric values", function() {
        var currencyLabel = "EUR";

        var ticketItem = new TicketItem();
        ticketItem.DiscountRate = 50;
        
        ticketItem.UnitPriceBrutto = new Money();
        ticketItem.UnitPriceBrutto.Amount = 0.15;
        ticketItem.UnitPriceBrutto.CurrencyLabel = currencyLabel;

        ticketItem.Quantity = new Quantity();
        ticketItem.Quantity.Amount = 1;
        ticketItem.Quantity.Unit = "ks";
        
        ticketItem.Plu = new TicketItemPlu();
        ticketItem.Plu.IsDiscountAllowed = true;

        var normalizedDiscountRate = ticketItem.getNormalizedDiscountRate();
        var nettoUnitPrice = ticketItem.getNettoUnitPrice();
        var totalBruttoPrice = ticketItem.getTotalBruttoPrice();
        var totalNettoPrice = ticketItem.getTotalNettoPrice();
        var discountAmount = ticketItem.getDiscountAmount();

        it("should have normalized discount rate of type number", function() {
            expect(normalizedDiscountRate).to.be.an('number');
        });
        it("should have normalized discount rate set to 0.5", function() {
            expect(normalizedDiscountRate).to.equal(0.5);
        });
        it("should have netto unit price of type object", function() {
            expect(nettoUnitPrice).to.be.an('object');
        });
        it("should have netto unit price amount set to 0.07", function() {
            expect(nettoUnitPrice.Amount).to.equal(0.07);
        });
        it("should have netto unit price currency label set correctly", function() {
            expect(nettoUnitPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have total brutto price of type object", function() {
            expect(totalBruttoPrice).to.be.an('object');
        });
        it("should have total brutto price amount set to 0.15", function() {
            expect(totalBruttoPrice.Amount).to.equal(0.15);
        });
        it("should have total brutto price currency label set correctly", function() {
            expect(totalBruttoPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have total netto price of type object", function() {
            expect(totalNettoPrice).to.be.an('object');
        });
        it("should have total netto price amount set to 0.07", function() {
            expect(totalNettoPrice.Amount).to.equal(0.07);
        });
        it("should have total netto price currency label set correctly", function() {
            expect(totalNettoPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have discount amount of type object", function() {
            expect(discountAmount).to.be.an('object');
        });
        it("should have discount amount amount set to 0.08", function() {
            expect(discountAmount.Amount).to.equal(0.08);
        });
        it("should have discount amount currency label set correctly", function() {
            expect(discountAmount.CurrencyLabel).to.equal(currencyLabel);
        });
    });

    describe("Discounted ticket item with rounding down", function() {
        var currencyLabel = "EUR";

        var ticketItem = new TicketItem();
        ticketItem.DiscountRate = 5;
        
        ticketItem.UnitPriceBrutto = new Money();
        ticketItem.UnitPriceBrutto.Amount = 1.5;
        ticketItem.UnitPriceBrutto.CurrencyLabel = currencyLabel;

        ticketItem.Quantity = new Quantity();
        ticketItem.Quantity.Amount = 0.3;
        ticketItem.Quantity.Unit = "ks";
        
        ticketItem.Plu = new TicketItemPlu();
        ticketItem.Plu.IsDiscountAllowed = true;

        var normalizedDiscountRate = ticketItem.getNormalizedDiscountRate();
        var nettoUnitPrice = ticketItem.getNettoUnitPrice();
        var totalBruttoPrice = ticketItem.getTotalBruttoPrice();
        var totalNettoPrice = ticketItem.getTotalNettoPrice();
        var discountAmount = ticketItem.getDiscountAmount();

        it("should have normalized discount rate of type number", function() {
            expect(normalizedDiscountRate).to.be.an('number');
        });
        it("should have normalized discount rate set to 0.05", function() {
            expect(normalizedDiscountRate).to.equal(0.05);
        });
        it("should have netto unit price of type object", function() {
            expect(nettoUnitPrice).to.be.an('object');
        });
        it("should have netto unit price amount set to 1.42", function() {
            expect(nettoUnitPrice.Amount).to.equal(1.42);
        });
        it("should have netto unit price currency label set correctly", function() {
            expect(nettoUnitPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have total brutto price of type object", function() {
            expect(totalBruttoPrice).to.be.an('object');
        });
        it("should have total brutto price amount set to 0.45", function() {
            expect(totalBruttoPrice.Amount).to.equal(0.45);
        });
        it("should have total brutto price currency label set correctly", function() {
            expect(totalBruttoPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have total netto price of type object", function() {
            expect(totalNettoPrice).to.be.an('object');
        });
        it("should have total netto price amount set to 0.43", function() {
            expect(totalNettoPrice.Amount).to.equal(0.43);
        });
        it("should have total netto price currency label set correctly", function() {
            expect(totalNettoPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have discount amount of type object", function() {
            expect(discountAmount).to.be.an('object');
        });
        it("should have discount amount amount set to 0.02", function() {
            expect(discountAmount.Amount).to.equal(0.02);
        });
        it("should have discount amount currency label set correctly", function() {
            expect(discountAmount.CurrencyLabel).to.equal(currencyLabel);
        });
    });

    describe("Discounted ticket item with rounding up", function() {
        var currencyLabel = "EUR";

        var ticketItem = new TicketItem();
        ticketItem.DiscountRate = 5;
        
        ticketItem.UnitPriceBrutto = new Money();
        ticketItem.UnitPriceBrutto.Amount = 2.9;
        ticketItem.UnitPriceBrutto.CurrencyLabel = currencyLabel;

        ticketItem.Quantity = new Quantity();
        ticketItem.Quantity.Amount = 1;
        ticketItem.Quantity.Unit = "ks";
        
        ticketItem.Plu = new TicketItemPlu();
        ticketItem.Plu.IsDiscountAllowed = true;

        var normalizedDiscountRate = ticketItem.getNormalizedDiscountRate();
        var nettoUnitPrice = ticketItem.getNettoUnitPrice();
        var totalBruttoPrice = ticketItem.getTotalBruttoPrice();
        var totalNettoPrice = ticketItem.getTotalNettoPrice();
        var discountAmount = ticketItem.getDiscountAmount();

        it("should have normalized discount rate of type number", function() {
            expect(normalizedDiscountRate).to.be.an('number');
        });
        it("should have normalized discount rate set to 0.05", function() {
            expect(normalizedDiscountRate).to.equal(0.05);
        });
        it("should have netto unit price of type object", function() {
            expect(nettoUnitPrice).to.be.an('object');
        });
        it("should have netto unit price amount set to 2.75", function() {
            expect(nettoUnitPrice.Amount).to.equal(2.75);
        });
        it("should have netto unit price currency label set correctly", function() {
            expect(nettoUnitPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have total brutto price of type object", function() {
            expect(totalBruttoPrice).to.be.an('object');
        });
        it("should have total brutto price amount set to 2.90", function() {
            expect(totalBruttoPrice.Amount).to.equal(2.90);
        });
        it("should have total brutto price currency label set correctly", function() {
            expect(totalBruttoPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have total netto price of type object", function() {
            expect(totalNettoPrice).to.be.an('object');
        });
        it("should have total netto price amount set to 2.75", function() {
            expect(totalNettoPrice.Amount).to.equal(2.75);
        });
        it("should have total netto price currency label set correctly", function() {
            expect(totalNettoPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have discount amount of type object", function() {
            expect(discountAmount).to.be.an('object');
        });
        it("should have discount amount amount set to 0.15", function() {
            expect(discountAmount.Amount).to.equal(0.15);
        });
        it("should have discount amount currency label set correctly", function() {
            expect(discountAmount.CurrencyLabel).to.equal(currencyLabel);
        });
    });

    describe("Ticket item with quantity equals to one", function() {
        var currencyLabel = "EUR";

        var ticketItem = new TicketItem();
        ticketItem.DiscountRate = 5;
        
        ticketItem.UnitPriceBrutto = new Money();
        ticketItem.UnitPriceBrutto.Amount = 2.9;
        ticketItem.UnitPriceBrutto.CurrencyLabel = currencyLabel;

        ticketItem.Quantity = new Quantity();
        ticketItem.Quantity.Amount = 1;
        ticketItem.Quantity.Unit = "ks";
        
        ticketItem.Plu = new TicketItemPlu();
        ticketItem.Plu.IsDiscountAllowed = true;

        var nettoUnitPrice = ticketItem.getNettoUnitPrice();
        var totalNettoPrice = ticketItem.getTotalNettoPrice();

        it("should have netto unit price amount equal to total netto price", function() {
            expect(nettoUnitPrice.Amount).to.equal(totalNettoPrice.Amount);
        });
    });

    describe("Discounted ticket item with quantity higher than one", function() {
        var currencyLabel = "EUR";

        var ticketItem = new TicketItem();
        ticketItem.DiscountRate = 30;
        
        ticketItem.UnitPriceBrutto = new Money();
        ticketItem.UnitPriceBrutto.Amount = 0.65;
        ticketItem.UnitPriceBrutto.CurrencyLabel = currencyLabel;

        ticketItem.Quantity = new Quantity();
        ticketItem.Quantity.Amount = 2;
        ticketItem.Quantity.Unit = "ks";
        
        ticketItem.Plu = new TicketItemPlu();
        ticketItem.Plu.IsDiscountAllowed = true;

        var normalizedDiscountRate = ticketItem.getNormalizedDiscountRate();
        var nettoUnitPrice = ticketItem.getNettoUnitPrice();
        var totalBruttoPrice = ticketItem.getTotalBruttoPrice();
        var totalNettoPrice = ticketItem.getTotalNettoPrice();
        var discountAmount = ticketItem.getDiscountAmount();

        it("should have normalized discount rate of type number", function() {
            expect(normalizedDiscountRate).to.be.an('number');
        });
        it("should have normalized discount rate set to 0.3", function() {
            expect(normalizedDiscountRate).to.equal(0.3);
        });
        it("should have netto unit price of type object", function() {
            expect(nettoUnitPrice).to.be.an('object');
        });
        it("should have netto unit price amount set to 0.45", function() {
            expect(nettoUnitPrice.Amount).to.equal(0.45);
        });
        it("should have netto unit price currency label set correctly", function() {
            expect(nettoUnitPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have total brutto price of type object", function() {
            expect(totalBruttoPrice).to.be.an('object');
        });
        it("should have total brutto price amount set to 1.3", function() {
            expect(totalBruttoPrice.Amount).to.equal(1.3);
        });
        it("should have total brutto price currency label set correctly", function() {
            expect(totalBruttoPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have total netto price of type object", function() {
            expect(totalNettoPrice).to.be.an('object');
        });
        it("should have total netto price amount set to 0.9", function() {
            expect(totalNettoPrice.Amount).to.equal(0.9);
        });
        it("should have total netto price currency label set correctly", function() {
            expect(totalNettoPrice.CurrencyLabel).to.equal(currencyLabel);
        });
        it("should have discount amount of type object", function() {
            expect(discountAmount).to.be.an('object');
        });
        it("should have discount amount amount set to 0.4", function() {
            expect(discountAmount.Amount).to.equal(0.4);
        });
        it("should have discount amount currency label set correctly", function() {
            expect(discountAmount.CurrencyLabel).to.equal(currencyLabel);
        });
    });
});