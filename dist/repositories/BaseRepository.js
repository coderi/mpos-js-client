"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var axios_1 = require("axios");
var class_transformer_1 = require("class-transformer");
var Exception_1 = require("../models/Exception");
var Localize_1 = require("../locales/Localize");
function dataToModel(value, data) {
    return class_transformer_1.plainToClass(value, data);
}
exports.dataToModel = dataToModel;
function dataToCollection(value, data) {
    return class_transformer_1.plainToClass(value, data);
}
exports.dataToCollection = dataToCollection;
function processError(error, onError) {
    var exception;
    if (error.name && error.name === "TypeError") {
        exception = new Exception_1.default();
        exception.Message = Localize_1.default("error.typeerror.message");
        exception.Code = 0;
    }
    else if (error.message && error.message === "Network Error") {
        exception = new Exception_1.default();
        exception.Message = Localize_1.default("error.network.message");
        exception.Code = 0;
    }
    else if (error.response) {
        if (error.response.data && error.response.data !== '') {
            exception = dataToModel(Exception_1.default, error.response.data);
        }
        else if (error.response.status) {
            switch (error.response.status) {
                case 404:
                    exception = new Exception_1.default();
                    exception.Message = Localize_1.default("error.404.message");
                    exception.Code = 0;
                    break;
                default:
                    exception = new Exception_1.default();
                    exception.Message = Localize_1.default("error.unknown.message");
                    exception.Code = 0;
                    break;
            }
        }
    }
    else {
        exception = new Exception_1.default();
        exception.Message = error.message;
        exception.Code = 0;
    }
    onError(exception, error);
}
function remapValidationErrors(errors, parent) {
    var exceptions = [];
    for (var _i = 0, errors_1 = errors; _i < errors_1.length; _i++) {
        var error = errors_1[_i];
        var field = (parent ? parent : "") + error.property;
        for (var rule in error.constraints) {
            var exception = new Exception_1.ValidationException();
            exception.Field = field;
            exception.Code = Exception_1.ExceptionCodes.Unspecified;
            exception.Message = error.constraints[rule];
            exceptions.push(exception);
        }
        if (error.children && error.children.length > 0) {
            var subExceptions = remapValidationErrors(error.children, field + ".");
            exceptions = exceptions.concat(subExceptions);
        }
    }
    return exceptions;
}
function sendRequest(method, uri, data, onSuccess, onError) {
    var request;
    switch (method) {
        case "GET":
            request = axios_1.default.get(uri);
            break;
        case "POST":
            request = axios_1.default.post(uri, data);
            break;
        case "PUT":
            request = axios_1.default.put(uri, data);
            break;
        case "DELETE":
            request = axios_1.default.delete(uri);
            break;
        default:
            var exception = new Exception_1.default();
            exception.Message = Localize_1.default("error.invalidRequest.message");
            exception.Code = Exception_1.ExceptionCodes.Unspecified;
            onError(exception, {});
            return;
    }
    request
        .then(function (response) {
        onSuccess(response.data, response);
    })
        .catch(function (error) {
        processError(error, onError);
    });
}
exports.sendRequest = sendRequest;
var RestRepository = (function () {
    function RestRepository(resourceUri) {
        this.resourceUri = "/api" + resourceUri;
    }
    RestRepository.prototype.send = function (method, path, data, onSuccess, onError) {
        sendRequest(method, this.resourceUri + path, data, onSuccess, onError);
    };
    return RestRepository;
}());
exports.RestRepository = RestRepository;
var OdataRepository = (function () {
    function OdataRepository(resourceUri, model) {
        this.resourceUri = "/odata" + resourceUri;
        this.model = model;
    }
    OdataRepository.prototype.send = function (method, path, data, onSuccess, onError) {
        sendRequest(method, this.resourceUri + path, data, onSuccess, onError);
    };
    OdataRepository.prototype.sendToId = function (id, method, path, data, onSuccess, onError) {
        sendRequest(method, this.resourceUri + "('" + id + "')" + path, data, onSuccess, onError);
    };
    return OdataRepository;
}());
exports.OdataRepository = OdataRepository;
var ROdataRepository = (function (_super) {
    __extends(ROdataRepository, _super);
    function ROdataRepository() {
        return _super.apply(this, arguments) || this;
    }
    ROdataRepository.prototype.findAll = function (onSuccess, onError) {
        var _this = this;
        axios_1.default.get(this.resourceUri)
            .then(function (response) {
            var collection = dataToCollection(_this.model, response.data.value);
            onSuccess(collection, response);
        })
            .catch(function (error) {
            processError(error, onError);
        });
    };
    ROdataRepository.prototype.findById = function (id, onSuccess, onError) {
        var _this = this;
        axios_1.default.get(this.resourceUri + "('" + id + "')")
            .then(function (response) {
            var model = dataToModel(_this.model, response.data);
            onSuccess(model, response);
        })
            .catch(function (error) {
            processError(error, onError);
        });
    };
    ROdataRepository.prototype.find = function (options, onSuccess, onError) {
        var _this = this;
        var parameters = [];
        if (options.hasOwnProperty("filter")) {
            parameters.push("$filter=" + options.filter);
        }
        if (options.hasOwnProperty("sort") && options.sort.column !== "") {
            parameters.push("$orderby=" + options.sort.column + " " + (options.sort.asc ? "asc" : "desc"));
        }
        if (options.hasOwnProperty("paginate")) {
            parameters.push("$skip=" + (options.paginate.page - 1) * options.paginate.limit);
            parameters.push("$top=" + options.paginate.limit);
        }
        var url = this.resourceUri + "?" + parameters.join("&");
        axios_1.default.get(url)
            .then(function (response) {
            var collection = dataToCollection(_this.model, response.data.value);
            onSuccess(collection, response);
        })
            .catch(function (error) {
            processError(error, onError);
        });
    };
    return ROdataRepository;
}(OdataRepository));
exports.ROdataRepository = ROdataRepository;
var RDOdataRepository = (function (_super) {
    __extends(RDOdataRepository, _super);
    function RDOdataRepository() {
        return _super.apply(this, arguments) || this;
    }
    RDOdataRepository.prototype.delete = function (id, onSuccess, onError) {
        var _this = this;
        axios_1.default.delete(this.resourceUri + "('" + id + "')")
            .then(function (response) {
            var model = dataToModel(_this.model, response.data);
            onSuccess(model, response);
        })
            .catch(function (error) {
            processError(error, onError);
        });
    };
    return RDOdataRepository;
}(ROdataRepository));
exports.RDOdataRepository = RDOdataRepository;
var RUDOdataRepository = (function (_super) {
    __extends(RUDOdataRepository, _super);
    function RUDOdataRepository() {
        return _super.apply(this, arguments) || this;
    }
    RUDOdataRepository.prototype.update = function (id, model, onSuccess, onError) {
        var _this = this;
        var data = class_transformer_1.classToPlain(model);
        axios_1.default.put(this.resourceUri + "('" + id + "')", data)
            .then(function (response) {
            var model = dataToModel(_this.model, response.data);
            onSuccess(model, response);
        })
            .catch(function (error) {
            processError(error, onError);
        });
    };
    return RUDOdataRepository;
}(RDOdataRepository));
exports.RUDOdataRepository = RUDOdataRepository;
var CRUDOdataRepository = (function (_super) {
    __extends(CRUDOdataRepository, _super);
    function CRUDOdataRepository() {
        return _super.apply(this, arguments) || this;
    }
    CRUDOdataRepository.prototype.create = function (model, onSuccess, onError) {
        var _this = this;
        var data = class_transformer_1.classToPlain(model);
        axios_1.default.post(this.resourceUri, data)
            .then(function (response) {
            var model = dataToModel(_this.model, response.data);
            onSuccess(model, response);
        })
            .catch(function (error) {
            processError(error, onError);
        });
    };
    return CRUDOdataRepository;
}(RUDOdataRepository));
exports.CRUDOdataRepository = CRUDOdataRepository;
//# sourceMappingURL=BaseRepository.js.map