"use strict";
var dict_1 = require("./dict");
var Localize = (function () {
    function Localize() {
    }
    Localize.setLocale = function (locale) {
        Localize.locale = locale;
    };
    return Localize;
}());
Localize.locale = "sk-SK";
exports.Localize = Localize;
function translate(key, params) {
    var message = (dict_1.default.hasOwnProperty(Localize.locale) && dict_1.default[Localize.locale].hasOwnProperty(key) ? dict_1.default[Localize.locale][key] : key);
    if (params && params.length > 0) {
        for (var i = 0; i < params.length; i++) {
            message = message.replace("{" + i + "}", params[i]);
        }
    }
    return message;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = translate;
//# sourceMappingURL=Localize.js.map