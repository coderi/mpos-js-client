import BaseModel from "./BaseModel";
import Device from "./Device";
export default class UserSessionProfile extends BaseModel {
    Name: string;
    UserName: string;
    Rights: string[];
    Device: Device;
}
