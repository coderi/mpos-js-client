export default class MPos {
    static initialize(apiUrl: string): void;
    static setAccessToken(accessToken: string): void;
    static removeAccessToken(): void;
    static setLanguage(language: string): void;
    static removeLanguage(): void;
}
