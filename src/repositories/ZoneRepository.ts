import { CRUDOdataRepository } from "./BaseRepository";

import Zone from "../models/Zone";

export default class ZoneRepository extends CRUDOdataRepository<Zone, string> { // Name
    constructor() {
        super("/Zones", Zone);
    }
}
