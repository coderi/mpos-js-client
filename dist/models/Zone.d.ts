import BaseModel from "./BaseModel";
import ZoneItem from "./ZoneItem";
export default class Zone extends BaseModel {
    Name: string;
    DefaultFiscalName: string;
    Items: ZoneItem[];
}
