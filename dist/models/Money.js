"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_validator_1 = require("class-validator");
var _ = require("lodash");
var BaseModel_1 = require("./BaseModel");
var Money = (function (_super) {
    __extends(Money, _super);
    function Money() {
        var _this = _super.apply(this, arguments) || this;
        _this.Amount = 0;
        return _this;
    }
    Money.prototype.add = function (value) {
        var result = new Money();
        var amount = this.roundAmount(this.Amount + value.Amount);
        result.Amount = amount;
        result.CurrencyLabel = this.CurrencyLabel;
        return result;
    };
    Money.prototype.subtract = function (value) {
        var result = new Money();
        var amount = this.roundAmount(this.Amount - value.Amount);
        result.Amount = amount;
        result.CurrencyLabel = this.CurrencyLabel;
        return result;
    };
    Money.prototype.applyDiscount = function (normalizedDiscountRate) {
        var result = new Money();
        var delta = this.roundAmount(this.Amount * normalizedDiscountRate);
        var amount = this.roundAmount(this.Amount - delta);
        result.Amount = amount;
        result.CurrencyLabel = this.CurrencyLabel;
        return result;
    };
    Money.prototype.multiply = function (value) {
        var result = new Money();
        var amount = this.roundAmount(this.Amount * value.Amount);
        result.Amount = amount;
        result.CurrencyLabel = this.CurrencyLabel;
        return result;
    };
    Money.prototype.divide = function (value) {
        var result = new Money();
        var amount = this.roundAmount(this.Amount / value.Amount);
        result.Amount = amount;
        result.CurrencyLabel = this.CurrencyLabel;
        return result;
    };
    Money.prototype.multiplyByNumber = function (value) {
        var result = new Money();
        var amount = this.roundAmount(this.Amount * value);
        result.Amount = amount;
        result.CurrencyLabel = this.CurrencyLabel;
        return result;
    };
    Money.prototype.divideByNumber = function (value) {
        var result = new Money();
        var amount = this.roundAmount(this.Amount / value);
        result.Amount = amount;
        result.CurrencyLabel = this.CurrencyLabel;
        return result;
    };
    Money.prototype.roundAmount = function (value) {
        return _.round(value, 2);
    };
    return Money;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], Money.prototype, "Amount", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString(),
    class_validator_1.Length(3)
], Money.prototype, "CurrencyLabel", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Money;
//# sourceMappingURL=Money.js.map