import { Type, Transform } from "class-transformer";

import { IsDefined, IsString, IsArray, IsDate, Length, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import UserInfo from "./UserInfo";
import StockTakingItem from "./StockTakingItem";

export default class StockTaking extends BaseModel {
    @IsString()
    Id             : string;

    @IsDefined()
    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    CreatedAt      : Date;

    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    ClosedAt       : Date;

    @IsDefined()
    @Type(() => UserInfo)
    CreatedBy      : UserInfo;

    @Type(() => UserInfo)
    ClosedBy       : UserInfo;

    @IsDefined()
    @IsString()
    @Length(3)
    CurrencyLabel      : string;

    @ValidateNested()
    @IsDefined()
    @IsArray()
    @Type(() => StockTakingItem)
    Items          : StockTakingItem[];
}
