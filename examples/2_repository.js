require("./init");

require("./autologin");

/*
 *  Dependencies
 */
var MPosLib = require("../dist/index"); // In production: require("mpos-lib")

var ArticleCategoryRepository = MPosLib.ArticleCategoryRepository;
var ArticleCategory = MPosLib.ArticleCategory;

/*
 *  Test Repository
 */
var articleCategoryRepository = new ArticleCategoryRepository();

// Find all
articleCategoryRepository.findAll(
    function (collection, response) {
        console.log("Success");
        console.log(collection);
    },
    function (error, response) {
        console.log("Error");
        console.log(error);
    }
);

// Find
/*
articleCategoryRepository.find(
    {
        filter: "Description eq 'A'",
        sort: {
            column: "Description",
            asc: true
        },
        paginate: {
            page: 1,
            limit: 3
        }
    },
    function (collection, response) {
        console.log("Success");
        console.log(collection);
    },
    function (error, response) {
        console.log("Error");
        console.log(error);
    }
);
*/

// Find By Id
/*
articleCategoryRepository.findById(
    "ALK",
    function (model, response) {
        console.log("Success");
        console.log(model);
    },
    function (error, response) {
        console.log("Error");
        console.log(error);
    }
);
*/

// Create
/*
var newModel = new ArticleCategory();
    newModel.Label = "TNT";
    newModel.Description = "Tri Nitro Toluen";

articleCategoryRepository.create(
    newModel,
    function (model, response) {
        console.log("Success");
        console.log(model);
    },
    function (error, response) {
        console.log("Error");
        console.log(error);
    }
);
*/

// Update
/*
var oldModel = new ArticleCategory();
    oldModel.Label = "TNT";
    oldModel.Description = "Tri Nitro Tolueen";

articleCategoryRepository.update(
    "TNT",
    oldModel,
    function (model, response) {
        console.log("Success");
        console.log(model);
    },
    function (error, response) {
        console.log("Error");
        console.log(error);
    }
);
*/

// Remove
/*
articleCategoryRepository.delete(
    "TNT",
    function (model, response) {
        console.log("Success");
        console.log(model);
    },
    function (error, response) {
        console.log("Error");
        console.log(error);
    }
);
*/
