import { Type, Transform } from "class-transformer";

import { IsDefined, IsString, IsNumber, IsArray, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import OrderItem from "./OrderItem";
import OrderTicketInfo from "./OrderTicketInfo";
import UserInfo from "./UserInfo";

export default class Order extends BaseModel {
    @IsDefined()
    @IsString()
    DeviceName  : string;

    @IsString()
    Id          : string;

    @IsDefined()
    @IsNumber()
    Number      : number;

    @ValidateNested()
    @IsDefined()
    @IsArray()
    @Type(() => OrderItem)
    Items       : OrderItem[];

    @ValidateNested()
    @IsDefined()
    @Type(() => OrderTicketInfo)
    TicketInfo  : OrderTicketInfo;

    @IsDefined()
    @Type(() => UserInfo)
    UserInfo    : UserInfo;
}
