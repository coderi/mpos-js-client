"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var BaseModel_1 = require("./BaseModel");
var ApiKey = (function (_super) {
    __extends(ApiKey, _super);
    function ApiKey() {
        return _super.apply(this, arguments) || this;
    }
    return ApiKey;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsString()
], ApiKey.prototype, "Id", void 0);
__decorate([
    class_validator_1.IsString()
], ApiKey.prototype, "ClientId", void 0);
__decorate([
    class_validator_1.IsString()
], ApiKey.prototype, "ClientSecret", void 0);
__decorate([
    class_validator_1.IsBoolean(),
    class_validator_1.IsDefined()
], ApiKey.prototype, "IsActive", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsDefined()
], ApiKey.prototype, "Name", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsDefined()
], ApiKey.prototype, "Description", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsDefined(),
    class_validator_1.IsUrl()
], ApiKey.prototype, "CallbackUrl", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsUrl()
], ApiKey.prototype, "HomepageUrl", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsDefined()
], ApiKey.prototype, "UserName", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsDefined()
], ApiKey.prototype, "DeviceName", void 0);
__decorate([
    class_validator_1.IsArray()
], ApiKey.prototype, "Permissions", void 0);
__decorate([
    class_validator_1.IsDate(),
    class_transformer_1.Type(function () { return Date; }),
    class_transformer_1.Transform(function (value) { return value.toISOString(); }, { toPlainOnly: true })
], ApiKey.prototype, "CreatedAt", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ApiKey;
//# sourceMappingURL=ApiKey.js.map