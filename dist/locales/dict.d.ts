declare var _default: {
    "sk-SK": {
        "error.unknown.message": string;
        "error.404.message": string;
        "error.network.message": string;
        "error.typeerror.message": string;
        "error.invalidRequest.message": string;
        "error.invalidtype.message": string;
        "error.invalid.message": string;
        "error.invalidChildren.message": string;
        "error.validation.isString": string;
        "error.validation.isBoolean": string;
        "error.validation.isNumber": string;
        "error.validation.isArray": string;
        "error.validation.isDate": string;
        "error.validation.isUrl": string;
        "error.validation.IsDefined": string;
        "error.validation.isHexColor": string;
        "error.validation.min": string;
        "error.validation.max": string;
        "error.validation.length": string;
        "error.validation.maxLength": string;
    };
};
export default _default;
