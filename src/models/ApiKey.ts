import { Type, Transform } from "class-transformer";

import { IsDefined, IsUrl, IsString, IsBoolean, IsDate, IsArray } from "class-validator";

import BaseModel from "./BaseModel";

export default class ApiKey extends BaseModel {
    @IsString()
    Id              : string;

    @IsString()
    ClientId        : string;

    @IsString()
    ClientSecret    : string;

    @IsBoolean()
    @IsDefined()
    IsActive        : boolean;

    @IsString()
    @IsDefined()
    Name            : string;

    @IsString()
    @IsDefined()
    Description     : string;

    @IsString()
    @IsDefined()
    @IsUrl()
    CallbackUrl     : string;

    @IsString()
    @IsUrl()
    HomepageUrl     : string;

    @IsString()
    @IsDefined()
    UserName        : string;

    @IsString()
    @IsDefined()
    DeviceName      : string;

    @IsArray()
    Permissions     : string[];

    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    CreatedAt       : Date;
}
