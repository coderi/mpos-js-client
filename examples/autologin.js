/*
 *  Dependencies
 */
var MPosLib = require("../dist/index"); // In production: require("mpos-lib")

var MPos = MPosLib.default;

/*
 *  Preauthorize
 */
var accessToken = "ACCESS_TOKEN";
MPos.setAccessToken(accessToken);
