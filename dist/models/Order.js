"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var BaseModel_1 = require("./BaseModel");
var OrderItem_1 = require("./OrderItem");
var OrderTicketInfo_1 = require("./OrderTicketInfo");
var UserInfo_1 = require("./UserInfo");
var Order = (function (_super) {
    __extends(Order, _super);
    function Order() {
        return _super.apply(this, arguments) || this;
    }
    return Order;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], Order.prototype, "DeviceName", void 0);
__decorate([
    class_validator_1.IsString()
], Order.prototype, "Id", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], Order.prototype, "Number", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_validator_1.IsArray(),
    class_transformer_1.Type(function () { return OrderItem_1.default; })
], Order.prototype, "Items", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return OrderTicketInfo_1.default; })
], Order.prototype, "TicketInfo", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return UserInfo_1.default; })
], Order.prototype, "UserInfo", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Order;
//# sourceMappingURL=Order.js.map