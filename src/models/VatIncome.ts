import { Type } from "class-transformer";

import { IsDefined, IsNumber, Min, Max, ValidateNested } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import Money from "./Money";

export default class VatIncome extends BaseModel {
    @IsDefined()
    @IsNumber()
    @Min(1, {
        message: t("error.validation.min")
    })
    @Max(5, {
        message: t("error.validation.max")
    })
    VatCategory    : number;

    @IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    @Max(100, {
        message: t("error.validation.max")
    })
    VatRate        : number;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    TotalAmount : Money;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    VatAmount : Money;
}
