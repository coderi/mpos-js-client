import BaseModel from "./BaseModel";
import MoneyInfo from "./MoneyInfo";
import TicketPaymentTypeInfo from "./TicketPaymentTypeInfo";
export default class TicketPayment extends BaseModel {
    Amount: MoneyInfo;
    PaymentType: TicketPaymentTypeInfo;
}
