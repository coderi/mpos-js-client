import { Type } from "class-transformer";

import { IsDefined, IsString, IsNumber, IsNotEmpty, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import Quantity from "./Quantity";

export default class PluReceiptItem extends BaseModel {
    @IsDefined()
    @IsNumber()
    PluCode         : number;

    @IsString()
    @IsNotEmpty()
    PluStockName    : string;

    @ValidateNested()
    @IsDefined()
    @Type(() => Quantity)
    Quantity        : Quantity;
}
