import BaseModel from "./BaseModel";
import UserDevice from "./UserDevice";
import UserInfo from "./UserInfo";
export default class User extends BaseModel {
    IsActive: boolean;
    Name: string;
    UserName: string;
    HasPassword: boolean;
    Devices: UserDevice[];
    getInfo(): UserInfo;
}
