import BaseModel from "./BaseModel";
export default class UserInfo extends BaseModel {
    Name: string;
    UserName: string;
}
