import { IsDefined, IsNumber, Max, Min } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

export default class Coordinates extends BaseModel {
	@IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    @Max(1, {
        message: t("error.validation.max")
    })
    X : number;

	@IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    @Max(1, {
        message: t("error.validation.max")
    })
    Y : number;
}
