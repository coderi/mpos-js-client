"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var Zone_1 = require("../models/Zone");
var ZoneRepository = (function (_super) {
    __extends(ZoneRepository, _super);
    function ZoneRepository() {
        return _super.call(this, "/Zones", Zone_1.default) || this;
    }
    return ZoneRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ZoneRepository;
//# sourceMappingURL=ZoneRepository.js.map