/*
 *  Dependencies
 */
var MPosLib = require("../dist/index"); // In production: require("mpos-lib")

var MPos = MPosLib.default;

/*
 *  Config
 */
var serverUrl = "http://SERVER_ADDRESS:3000";

/*
 *  Initialize API provider - Needs to be set only ONCE! (static, global)
 */
MPos.initialize(serverUrl);
