"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var ArticleCategory_1 = require("../models/ArticleCategory");
var ArticleCategoryRepository = (function (_super) {
    __extends(ArticleCategoryRepository, _super);
    function ArticleCategoryRepository() {
        return _super.call(this, "/ArticleCategories", ArticleCategory_1.default) || this;
    }
    return ArticleCategoryRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ArticleCategoryRepository;
//# sourceMappingURL=ArticleCategoryRepository.js.map