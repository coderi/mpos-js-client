import BaseModel from "./BaseModel";
export default class ArticleCategory extends BaseModel {
    Color: string;
    CourseNumber: number;
    Description: string;
    Label: string;
}
