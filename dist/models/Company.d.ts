import BaseModel from "./BaseModel";
import Address from "./Address";
import BankAccount from "./BankAccount";
import Contact from "./Contact";
export default class Company extends BaseModel {
    Id: string;
    Name: string;
    CRN: string;
    VatId: string;
    TaxId: string;
    TaxPayer: boolean;
    Register: string;
    DueDays: number;
    Type: string;
    IsDefault: boolean;
    DiscountRate: number;
    Address: Address;
    ShippingAddress: Address;
    BankAccount: BankAccount;
    Contact: Contact;
}
