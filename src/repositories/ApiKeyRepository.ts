import { CRUDOdataRepository } from "./BaseRepository";

import ApiKey from "../models/ApiKey";

export default class ApiKeyRepository extends CRUDOdataRepository<ApiKey, string> { // Id
    constructor() {
        super("/ApiKeys", ApiKey);
    }
}
