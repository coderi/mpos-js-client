"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var Company_1 = require("../models/Company");
var CompanyRepository = (function (_super) {
    __extends(CompanyRepository, _super);
    function CompanyRepository() {
        return _super.call(this, "/Companies", Company_1.default) || this;
    }
    return CompanyRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = CompanyRepository;
//# sourceMappingURL=CompanyRepository.js.map