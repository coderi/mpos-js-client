import { IsNumber, IsDefined, IsString, IsBoolean, Length, Min } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

export default class Currency extends BaseModel {
    @IsDefined()
    @IsBoolean()
    IsDomestic      : boolean;

    @IsDefined()
    @IsString()
    @Length(3)
    Label           : string;

    @IsString()
    @Length(1)
    Sign            : string;

    @IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    ExchangeRate    : number;

    @IsDefined()
    @IsString()
    Description     : string;
}
