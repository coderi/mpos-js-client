import { Type, Transform } from "class-transformer";

import { IsDefined, IsString, IsNotEmpty, IsArray, IsNumber, IsDate, IsIn, Length, Max, Min, ValidateNested } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import Money from "./Money";
import MoneyInfo from "./MoneyInfo";
import TicketItem from "./TicketItem";
import TicketCloseInfo from "./TicketCloseInfo";
import TicketCustomerInfo from "./TicketCustomerInfo";
import TicketLocation from "./TicketLocation";
import TicketPayment from "./TicketPayment";
import Quantity from "./Quantity";
import UserInfo from "./UserInfo";

export default class Ticket extends BaseModel {
    @IsString()
    Id                 : string;

    @IsString()
    @IsNotEmpty()
    Name               : string;

    @IsDefined()
    @IsString()
    @IsIn(["Unknown", "WalkIn", "TakeAway", "Delivery"])
    PurchaseType       : string;

    @IsString()
    @IsNotEmpty()
    OriginDeviceName   : string;

    @IsDefined()
    @IsString()
    @Length(3)
    CurrencyLabel      : string;

    @IsDefined()
    @IsNumber()
    DiscountRate       : number;

    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    OpenDate           : Date;

    @ValidateNested()
    @Type(() => TicketCloseInfo)
    CloseInfo          : TicketCloseInfo;

    @ValidateNested()
    @Type(() => TicketLocation)
    Location           : TicketLocation;

    @Type(() => TicketCustomerInfo)
    Customer           : TicketCustomerInfo;

    @IsDefined()
    @Type(() => UserInfo)
    Employee           : UserInfo;

    @IsDefined()
    @IsArray()
    @Type(() => TicketItem)
    Items              : TicketItem[];

    @ValidateNested()
    @IsDefined()
    @IsArray()
    @Type(() => TicketPayment)
    Payments           : TicketPayment[];

    getNormalizedDiscountRate () : number {
        return this.DiscountRate / 100;
    }

    getTotalBruttoPrice () : Money {
        let money  = new Money();

        money.CurrencyLabel = this.CurrencyLabel;

        for (let item of this.Items) {
            money = money.add(item.getTotalBruttoPrice());
        }

        return money;
    }

    getTotalNettoPrice () : Money {
        let money  = new Money();
        let normalizedDiscountRate = this.getNormalizedDiscountRate();

        money.CurrencyLabel = this.CurrencyLabel;

        for (let item of this.Items) {
            if (item.Plu.IsDiscountAllowed) {
                let itemTotalNettoPrice = item.getTotalNettoPrice().applyDiscount(normalizedDiscountRate);
                money = money.add(itemTotalNettoPrice);
            } else {
                money = money.add(item.getTotalBruttoPrice());
            }
        }

        return money;
    }

    getDiscountAmount () : Money {
        return this.getTotalBruttoPrice().subtract(this.getTotalNettoPrice());
    }

    getTotalPaymentsAmount () : Money {
        let money  = new Money();

        money.CurrencyLabel = this.CurrencyLabel;

        for (let payment of this.Payments) {
            money = money.add(payment.Amount.toDomesticMoney());
        }

        return money;
    }

    /**
     * Checks if sum of payments is greater or equal to total netto price.
     */
    isPaid () : boolean {
        let totalNettoPrice = this.getTotalBruttoPrice();
        let totalPaymentsAmount = this.getTotalPaymentsAmount();

        return totalPaymentsAmount.Amount >= totalNettoPrice.Amount;
    }

    /**
     * Checks if items collection does not contain any element.
     */
    isEmpty () : boolean {
         return this.Items == null || this.Items.length == 0;
    }

    /**
     * Checks if ticket is already closed.
     */
    isClosed () : boolean {
        return this.CloseInfo != null;
    }

    /**
     * Checks if this ticket can be closed.
     */
    isCloseable () : boolean {
        return !this.isEmpty()
            && !this.isClosed()
            && this.isPaid();
    }
}
