import { IsString, IsDefined } from "class-validator";

import BaseModel from "./BaseModel";

export default class Address extends BaseModel {
	@IsDefined()
    @IsString()
    Street  : string;

	@IsDefined()
    @IsString()
    City    : string;

	@IsDefined()
    @IsString()
    ZipCode : string;

	@IsDefined()
    @IsString()
    Country : string;
}
