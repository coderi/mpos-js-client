import BaseModel from "./BaseModel";
export default class Address extends BaseModel {
    Street: string;
    City: string;
    ZipCode: string;
    Country: string;
}
