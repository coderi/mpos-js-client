import { CRUDOdataRepository } from "./BaseRepository";

import Plu from "../models/Plu";

export default class PluRepository extends CRUDOdataRepository<Plu, string> { // Code
    constructor() {
        super("/Plus", Plu);
    }
}
