import { IsDefined, IsString } from "class-validator";

import BaseModel from "./BaseModel";

export default class KeyValue extends BaseModel {
    @IsDefined()
    @IsString()
    Key     : string;

    @IsDefined()
    @IsString()
    Value   : string;
}
