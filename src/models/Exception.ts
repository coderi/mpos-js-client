import { Type } from "class-transformer";

export interface ExceptionCallback {
    (error: Exception, response: any): void;
}

export enum ExceptionCodes {
    Unspecified         = 0,
    Unauthenticated     = 1,
    Unauthorized        = 2,
    ValidationFailed    = 3,
    LicenceExpired      = 4,
    ResourceLocked      = 5
}
export default class Exception {
    Message : string;
    Code    : number;
    Errors  : ValidationException[];
}
export class ValidationException {
    Message : string;
    Code    : number;
    Field   : string;
}
