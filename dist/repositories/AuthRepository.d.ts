import { ExceptionCallback } from "../models/Exception";
import { RestRepository } from "./BaseRepository";
import UserSession from "../models/UserSession";
import UserSessionProfile from "../models/UserSessionProfile";
export default class AuthRepository extends RestRepository {
    constructor();
    login(username: string, password: string, deviceName: string, onSuccess: (session: UserSession, response: any) => void, onError: ExceptionCallback): void;
    logout(onSuccess: (response: any) => void, onError: ExceptionCallback): void;
    loadCurrentUser(onSuccess: (user: UserSessionProfile, response: any) => void, onError: ExceptionCallback): void;
}
