"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Licence_1 = require("../models/Licence");
var BaseRepository_1 = require("./BaseRepository");
var LicenceRepository = (function (_super) {
    __extends(LicenceRepository, _super);
    function LicenceRepository() {
        return _super.call(this, "/licence") || this;
    }
    LicenceRepository.prototype.getLicence = function (onSuccess, onError) {
        this.send("GET", "/", {}, function (data, response) {
            var licence = BaseRepository_1.dataToModel(Licence_1.default, data);
            onSuccess(licence, response);
        }, onError);
    };
    LicenceRepository.prototype.activateLicence = function (activationCode, onSuccess, onError) {
        this.send("POST", "/", activationCode, function (data, response) {
            var licence = BaseRepository_1.dataToModel(Licence_1.default, data);
            onSuccess(licence, response);
        }, onError);
    };
    LicenceRepository.prototype.getLicenceRequestCode = function (maxDevicesCount, onSuccess, onError) {
        var url = "/requestcode" + (maxDevicesCount ? ("?MaxDevicesCount=" + maxDevicesCount) : "");
        this.send("GET", url, {}, function (data, response) {
            onSuccess(data, response);
        }, onError);
    };
    return LicenceRepository;
}(BaseRepository_1.RestRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LicenceRepository;
//# sourceMappingURL=LicenceRepository.js.map