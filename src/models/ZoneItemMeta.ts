import { Type } from "class-transformer";

import { IsDefined, IsNumber, Min, Max, IsString, IsHexColor, IsIn, ValidateNested } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import Coordinates from "./Coordinates";

export default class ZoneItemMeta extends BaseModel {
    @IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    Height      : number;

    @IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    Width       : number;

    @IsDefined()
    @IsString()
    @IsIn(["Circle", "Rectangle"])
    Shape       : string;

    @IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    @Max(359, {
        message: t("error.validation.max")
    })
    Rotation    : number;

    @IsDefined()
    @IsString()
    @IsHexColor()
    Color       : string;

    @ValidateNested()
    @IsDefined()
    @Type(() => Coordinates)
    Position    : Coordinates;
}
