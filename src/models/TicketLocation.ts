import { IsDefined, IsString } from "class-validator";

import BaseModel from "./BaseModel";

export default class TicketLocation extends BaseModel {
    @IsDefined()
    @IsString()
    TableName   : string;

    @IsDefined()
    @IsString()
    ZoneName    : string;
}
