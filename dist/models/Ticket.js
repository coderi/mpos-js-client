"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var BaseModel_1 = require("./BaseModel");
var Money_1 = require("./Money");
var TicketItem_1 = require("./TicketItem");
var TicketCloseInfo_1 = require("./TicketCloseInfo");
var TicketCustomerInfo_1 = require("./TicketCustomerInfo");
var TicketLocation_1 = require("./TicketLocation");
var TicketPayment_1 = require("./TicketPayment");
var UserInfo_1 = require("./UserInfo");
var Ticket = (function (_super) {
    __extends(Ticket, _super);
    function Ticket() {
        return _super.apply(this, arguments) || this;
    }
    Ticket.prototype.getNormalizedDiscountRate = function () {
        return this.DiscountRate / 100;
    };
    Ticket.prototype.getTotalBruttoPrice = function () {
        var money = new Money_1.default();
        money.CurrencyLabel = this.CurrencyLabel;
        for (var _i = 0, _a = this.Items; _i < _a.length; _i++) {
            var item = _a[_i];
            money = money.add(item.getTotalBruttoPrice());
        }
        return money;
    };
    Ticket.prototype.getTotalNettoPrice = function () {
        var money = new Money_1.default();
        var normalizedDiscountRate = this.getNormalizedDiscountRate();
        money.CurrencyLabel = this.CurrencyLabel;
        for (var _i = 0, _a = this.Items; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.Plu.IsDiscountAllowed) {
                var itemTotalNettoPrice = item.getTotalNettoPrice().applyDiscount(normalizedDiscountRate);
                money = money.add(itemTotalNettoPrice);
            }
            else {
                money = money.add(item.getTotalBruttoPrice());
            }
        }
        return money;
    };
    Ticket.prototype.getDiscountAmount = function () {
        return this.getTotalBruttoPrice().subtract(this.getTotalNettoPrice());
    };
    Ticket.prototype.getTotalPaymentsAmount = function () {
        var money = new Money_1.default();
        money.CurrencyLabel = this.CurrencyLabel;
        for (var _i = 0, _a = this.Payments; _i < _a.length; _i++) {
            var payment = _a[_i];
            money = money.add(payment.Amount.toDomesticMoney());
        }
        return money;
    };
    Ticket.prototype.isPaid = function () {
        var totalNettoPrice = this.getTotalBruttoPrice();
        var totalPaymentsAmount = this.getTotalPaymentsAmount();
        return totalPaymentsAmount.Amount >= totalNettoPrice.Amount;
    };
    Ticket.prototype.isEmpty = function () {
        return this.Items == null || this.Items.length == 0;
    };
    Ticket.prototype.isClosed = function () {
        return this.CloseInfo != null;
    };
    Ticket.prototype.isCloseable = function () {
        return !this.isEmpty()
            && !this.isClosed()
            && this.isPaid();
    };
    return Ticket;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsString()
], Ticket.prototype, "Id", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty()
], Ticket.prototype, "Name", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString(),
    class_validator_1.IsIn(["Unknown", "WalkIn", "TakeAway", "Delivery"])
], Ticket.prototype, "PurchaseType", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty()
], Ticket.prototype, "OriginDeviceName", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString(),
    class_validator_1.Length(3)
], Ticket.prototype, "CurrencyLabel", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], Ticket.prototype, "DiscountRate", void 0);
__decorate([
    class_validator_1.IsDate(),
    class_transformer_1.Type(function () { return Date; }),
    class_transformer_1.Transform(function (value) { return value.toISOString(); }, { toPlainOnly: true })
], Ticket.prototype, "OpenDate", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_transformer_1.Type(function () { return TicketCloseInfo_1.default; })
], Ticket.prototype, "CloseInfo", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_transformer_1.Type(function () { return TicketLocation_1.default; })
], Ticket.prototype, "Location", void 0);
__decorate([
    class_transformer_1.Type(function () { return TicketCustomerInfo_1.default; })
], Ticket.prototype, "Customer", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return UserInfo_1.default; })
], Ticket.prototype, "Employee", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsArray(),
    class_transformer_1.Type(function () { return TicketItem_1.default; })
], Ticket.prototype, "Items", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_validator_1.IsArray(),
    class_transformer_1.Type(function () { return TicketPayment_1.default; })
], Ticket.prototype, "Payments", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Ticket;
//# sourceMappingURL=Ticket.js.map