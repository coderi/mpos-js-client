"use strict";
var class_validator_1 = require("class-validator");
var BaseModel = (function () {
    function BaseModel() {
    }
    BaseModel.prototype.isValid = function () {
        return (class_validator_1.validateSync(this, { validationError: { value: false }, skipMissingProperties: true }).length === 0);
    };
    return BaseModel;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BaseModel;
//# sourceMappingURL=BaseModel.js.map