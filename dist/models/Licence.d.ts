import BaseModel from "./BaseModel";
export default class Licence extends BaseModel {
    CertificateId: number;
    DKPs: string[];
    Modules: string[];
    MaxDevicesCount: number;
    RemainingDays: number;
    IsExpired: boolean;
    ActivationDate: Date;
    ExpirationDate: Date;
}
