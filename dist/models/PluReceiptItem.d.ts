import BaseModel from "./BaseModel";
import Quantity from "./Quantity";
export default class PluReceiptItem extends BaseModel {
    PluCode: number;
    PluStockName: string;
    Quantity: Quantity;
}
