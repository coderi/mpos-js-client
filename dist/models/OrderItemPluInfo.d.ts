import BaseModel from "./BaseModel";
export default class OrderItemPluInfo extends BaseModel {
    Code: number;
    Name: string;
    StockName: string;
    CourseNumber: number;
}
