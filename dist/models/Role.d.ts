import BaseModel from "./BaseModel";
export default class Role extends BaseModel {
    Name: string;
    Label: string;
    Description: string;
    Rights: string[];
}
