import { CRUDOdataRepository } from "./BaseRepository";
import { ExceptionCallback } from "../models/Exception";
import Fiscal from "../models/Fiscal";
import CashTransfer from "../models/CashTransfer";
import DailySalesReport from "../models/DailySalesReport";
export default class FiscalRepository extends CRUDOdataRepository<Fiscal, string> {
    constructor();
    openDrawer(id: string, onSuccess: (response: any) => void, onError: ExceptionCallback): void;
    createCashTransfer(id: string, cashTranfser: CashTransfer, onSuccess: (dailySalesReport: DailySalesReport, response: any) => void, onError: ExceptionCallback): void;
    printRecordCopy(id: string, dailySalesReportNumber: number, dailySalesReportDate: string, ticketNumber: number, onSuccess: (response: any) => void, onError: ExceptionCallback): void;
    printLastRecordCopy(id: string, onSuccess: (response: any) => void, onError: ExceptionCallback): void;
    printOverviewSalesReport(id: string, onSuccess: (response: any) => void, onError: ExceptionCallback): void;
    printDailySalesReport(id: string, onSuccess: (dailySalesReport: DailySalesReport, response: any) => void, onError: ExceptionCallback): void;
    printDailySalesReportCopy(id: string, dailySalesReportNumber: number, dailySalesReportDate: string, onSuccess: (response: any) => void, onError: ExceptionCallback): void;
    printLastDailySalesReportCopy(id: string, onSuccess: (response: any) => void, onError: ExceptionCallback): void;
    printSummaryIntervalSalesReport(id: string, dsrDateFrom: string, dsrDateTo: string, dsrNumberFrom: string, dsrNumberTo: string, onSuccess: (response: any) => void, onError: ExceptionCallback): void;
    printDetailedIntervalSalesReport(id: string, dsrDateFrom: string, dsrDateTo: string, dsrNumberFrom: string, dsrNumberTo: string, onSuccess: (response: any) => void, onError: ExceptionCallback): void;
    printNonfiscalRecord(id: string, body: string, onSuccess: (response: any) => void, onError: ExceptionCallback): void;
}
