"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var BaseModel_1 = require("./BaseModel");
var Money_1 = require("./Money");
var VatIncome_1 = require("./VatIncome");
var DailySalesReportFiscalInfo = (function (_super) {
    __extends(DailySalesReportFiscalInfo, _super);
    function DailySalesReportFiscalInfo() {
        return _super.apply(this, arguments) || this;
    }
    return DailySalesReportFiscalInfo;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DailySalesReportFiscalInfo.prototype, "TicketsCount", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], DailySalesReportFiscalInfo.prototype, "StornoAmount", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DailySalesReportFiscalInfo.prototype, "StornoCount", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], DailySalesReportFiscalInfo.prototype, "DiscountAmount", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DailySalesReportFiscalInfo.prototype, "DiscountCount", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], DailySalesReportFiscalInfo.prototype, "NegativeAmount", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DailySalesReportFiscalInfo.prototype, "NegativeCount", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], DailySalesReportFiscalInfo.prototype, "ReturnAmount", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DailySalesReportFiscalInfo.prototype, "ReturnCount", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], DailySalesReportFiscalInfo.prototype, "InvalidAmount", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DailySalesReportFiscalInfo.prototype, "InvalidCount", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], DailySalesReportFiscalInfo.prototype, "RoundingUp", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], DailySalesReportFiscalInfo.prototype, "RoundingDown", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsDate(),
    class_transformer_1.Type(function () { return Date; }),
    class_transformer_1.Transform(function (value) { return value.toISOString(); }, { toPlainOnly: true })
], DailySalesReportFiscalInfo.prototype, "CloseDate", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return VatIncome_1.default; })
], DailySalesReportFiscalInfo.prototype, "VatIncomes", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], DailySalesReportFiscalInfo.prototype, "GrandTotal", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = DailySalesReportFiscalInfo;
//# sourceMappingURL=DailySalesReportFiscalInfo.js.map