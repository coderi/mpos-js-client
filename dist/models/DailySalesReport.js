"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var BaseModel_1 = require("./BaseModel");
var CashTransfer_1 = require("./CashTransfer");
var DailySalesReportFiscalInfo_1 = require("./DailySalesReportFiscalInfo");
var DailySalesReport = (function (_super) {
    __extends(DailySalesReport, _super);
    function DailySalesReport() {
        return _super.apply(this, arguments) || this;
    }
    return DailySalesReport;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsString()
], DailySalesReport.prototype, "Id", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty()
], DailySalesReport.prototype, "FiscalName", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DailySalesReport.prototype, "Number", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], DailySalesReport.prototype, "FiscalMemorySerialNumber", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString(),
    class_validator_1.Length(3)
], DailySalesReport.prototype, "CurrencyLabel", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return DailySalesReportFiscalInfo_1.default; })
], DailySalesReport.prototype, "FiscalInfo", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsDate(),
    class_transformer_1.Type(function () { return Date; }),
    class_transformer_1.Transform(function (value) { return value.toISOString(); }, { toPlainOnly: true })
], DailySalesReport.prototype, "OpenDate", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsArray(),
    class_transformer_1.Type(function () { return CashTransfer_1.default; })
], DailySalesReport.prototype, "CashTransfers", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = DailySalesReport;
//# sourceMappingURL=DailySalesReport.js.map