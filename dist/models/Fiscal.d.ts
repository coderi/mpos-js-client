import BaseModel from "./BaseModel";
import KeyValue from "./KeyValue";
export default class Fiscal extends BaseModel {
    IsActive: boolean;
    Name: string;
    Type: string;
    DKP: string;
    Version: string;
    Settings: KeyValue[];
}
