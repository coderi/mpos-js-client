"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_validator_1 = require("class-validator");
var BaseModel_1 = require("./BaseModel");
var Contact = (function (_super) {
    __extends(Contact, _super);
    function Contact() {
        return _super.apply(this, arguments) || this;
    }
    return Contact;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], Contact.prototype, "Fax", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], Contact.prototype, "Phone", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], Contact.prototype, "Mobile", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], Contact.prototype, "Email", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Contact;
//# sourceMappingURL=Contact.js.map