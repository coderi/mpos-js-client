"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var Device_1 = require("../models/Device");
var DeviceRepository = (function (_super) {
    __extends(DeviceRepository, _super);
    function DeviceRepository() {
        return _super.call(this, "/Devices", Device_1.default) || this;
    }
    return DeviceRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = DeviceRepository;
//# sourceMappingURL=DeviceRepository.js.map