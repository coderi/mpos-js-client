import axios from "axios";

import { plainToClass, classToPlain } from "class-transformer";
import { validate, ValidationError } from "class-validator";

import Exception, { ValidationException, ExceptionCodes, ExceptionCallback } from "../models/Exception";

import t from "../locales/Localize";

/*
 *  Data to Model Serialization
 */
export function dataToModel<U>(value: any, data: Object) : U {
    return plainToClass(value, data) as U;
}
export function dataToCollection<U>(value: any, data: Object[]) : U[] {
    return plainToClass(value, data) as U[];
}

/*
 *  Error to Exception Serialization
 */
function processError (error: any, onError: ExceptionCallback) : void {
    let exception : Exception;

    if (error.name && error.name === "TypeError") {
        exception = new Exception ();

        exception.Message = t("error.typeerror.message");
        exception.Code = 0;
    }
    // Fixed: According axios error issue https://github.com/mzabriskie/axios/issues/383
    else if (error.message && error.message === "Network Error") {
        exception = new Exception ();

        exception.Message = t("error.network.message");
        exception.Code = 0;
    } else if (error.response) {
        if (error.response.data && error.response.data !== '') {
            exception = dataToModel<Exception>(Exception, error.response.data);
        } else if (error.response.status) {
            switch (error.response.status) {
                case 404:
                    exception = new Exception();

                    exception.Message = t("error.404.message");
                    exception.Code = 0;

                    break;
                default:
                    exception = new Exception();

                    exception.Message = t("error.unknown.message");
                    exception.Code = 0;

                    break;
            }
        }
    } else {
        exception = new Exception ();

        exception.Message = error.message;
        exception.Code = 0;
    }

    onError(exception, error);
}

/*
 *  Remap ValidationErrors to ValidationExceptions
 */
function remapValidationErrors (errors: ValidationError[], parent? :string) : ValidationException[] {
    let exceptions : ValidationException[] = [];

    for (let error of errors) {
        let field = (parent ? parent : "") + error.property;

        for (let rule in error.constraints) {
            let exception = new ValidationException();

            exception.Field     = field;
            exception.Code      = ExceptionCodes.Unspecified;
            exception.Message   = error.constraints[rule];

            exceptions.push(exception);
        }

        if (error.children && error.children.length > 0) {
            let subExceptions = remapValidationErrors(error.children, field + ".");

            exceptions = exceptions.concat(subExceptions);
        }
    }

    return exceptions;
}

/*
 *  Model Validation
 *
export function validateModel(model: any, type: any, onSuccess: () => void, onError: ExceptionCallback) {
    if (!(model instanceof type)) {
        let exception : Exception = new Exception();

        exception.Message = t("error.invalidtype.message");
        exception.Code    = ExceptionCodes.ValidationFailed;

        onError(exception, {});

        return;
    }

    validate(model, { validationError: { value: false }, skipMissingProperties: true }).then(errors => {
        if (errors.length > 0) {
            let exception : Exception = new Exception();

            exception.Message   = t("error.invalid.message");
            exception.Code      = ExceptionCodes.ValidationFailed;
            exception.Errors    = remapValidationErrors(errors);

            onError(exception, errors);

            return;
        }

        onSuccess();
    });
}
*/

/*
 *  Direct HTTP request
 */
export function sendRequest (
    method: string,
    uri:    string,
    data:   any,
    onSuccess: (data: any, response: any) => void,
    onError: ExceptionCallback
) : void {
    var request;

    switch (method) {
    case "GET":
        request = axios.get(uri);
        break;
    case "POST":
        request = axios.post(uri, data);
        break;
    case "PUT":
        request = axios.put(uri, data);
        break;
    case "DELETE":
        request = axios.delete(uri);
        break;
    default:
        let exception : Exception = new Exception();

        exception.Message   = t("error.invalidRequest.message");
        exception.Code      = ExceptionCodes.Unspecified;

        onError(exception, {});

        return;
    }

    request
        .then((response) : void => {
            onSuccess(response.data, response);
        })
        .catch((error) : void => {
            processError(error, onError);
        });
}

/*
 *  REST API Base Repository
 */
export class RestRepository {
    private resourceUri : string;

    constructor (resourceUri: string) {
        this.resourceUri = "/api" + resourceUri;
    }

    protected send (
        method: string,
        path:   string,
        data:   any,
        onSuccess: (data: any, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        sendRequest(method, this.resourceUri + path, data, onSuccess, onError);
    }
}

/*
 *  OData API Base Repository
 */
export class OdataRepository<U> {
    protected resourceUri  : string;
    protected model        : any;

    constructor (resourceUri: string, model: any) {
        this.resourceUri = "/odata" + resourceUri;
        this.model       = model;
    }

    protected send (
        method: string,
        path:   string,
        data:   any,
        onSuccess: (data: any, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        sendRequest(method, this.resourceUri + path, data, onSuccess, onError);
    }

    protected sendToId (
        id:     U,
        method: string,
        path:   string,
        data:   any,
        onSuccess: (data: any, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        sendRequest(method, this.resourceUri + "('" + id + "')" + path, data, onSuccess, onError);
    }
}

/*
 *  OData API Read Repository
 */
export class ROdataRepository<T, U> extends OdataRepository<U> {
    findAll (
        onSuccess: (collection: T[], response: any) => void,
        onError: ExceptionCallback
    ) : void {
        axios.get(this.resourceUri)
            .then((response) : void => {
                let collection = dataToCollection<T>(this.model, response.data.value);
                onSuccess(collection, response);
            })
            .catch((error) : void => {
                processError(error, onError);
            });
    }
    findById (
        id: U,
        onSuccess: (model: T, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        axios.get(this.resourceUri + "('" + id + "')")
            .then((response) : void => {
                let model = dataToModel<T>(this.model, response.data);
                onSuccess(model, response);
            })
            .catch((error) : void => {
                processError(error, onError);
            });
    }
    find (
        options: any,
        onSuccess: (collection: T[], response: any) => void,
        onError: ExceptionCallback
    ) : void {
        var parameters = [];

        // Filter
        if (options.hasOwnProperty("filter")) { // && options.filter.query !== "") {
            /*
            var filters = [];

            for (var i in options.filter.columns) {
                filters.push("contains(" + options.filter.columns[i] + ", '" + options.filter.query + "')");
            }

            parameters.push("$filter=" + filters.join(" or "));
            */
            parameters.push("$filter=" + options.filter);
        }

        // Sort
        if (options.hasOwnProperty("sort") && options.sort.column !== "") {
            parameters.push("$orderby=" + options.sort.column + " " + (options.sort.asc ? "asc" : "desc"));
        }

        // Paginate
        if (options.hasOwnProperty("paginate")) {
            parameters.push("$skip=" + (options.paginate.page-1)*options.paginate.limit);
            parameters.push("$top=" + options.paginate.limit);
        }

        let url = this.resourceUri + "?" + parameters.join("&");

        axios.get(url)
            .then((response) : void => {
                let collection = dataToCollection<T>(this.model, response.data.value);
                onSuccess(collection, response);
            })
            .catch((error) : void => {
                processError(error, onError);
            });
    }
}

/*
 *  OData API Read + Delete Repository
 */
export class RDOdataRepository<T, U> extends ROdataRepository<T, U> {
    delete (
        id: U,
        onSuccess: (model: T, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        axios.delete(this.resourceUri + "('" + id + "')")
            .then((response) : void => {
                let model = dataToModel<T>(this.model, response.data);
                onSuccess(model, response);
            })
            .catch((error) : void => {
                processError(error, onError);
            });
    }
}

/*
 *  OData API Read + Update + Delete Repository
 */
export class RUDOdataRepository<T, U> extends RDOdataRepository<T, U> {
    update (
        id: U,
        model: T,
        onSuccess: (model: T, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = classToPlain(model);

        axios.put(this.resourceUri + "('" + id + "')", data)
            .then((response) : void => {
                let model = dataToModel<T>(this.model, response.data);
                onSuccess(model, response);
            })
            .catch((error) : void => {
                processError(error, onError);
            });
    }
}

/*
 *  OData API Create + Read + Update + Delete Repository
 */
export class CRUDOdataRepository<T, U> extends RUDOdataRepository<T, U> {
    create (
        model: T,
        onSuccess: (model: T, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = classToPlain(model);

        axios.post(this.resourceUri, data)
            .then((response) : void => {
                let model = dataToModel<T>(this.model, response.data);
                onSuccess(model, response);
            })
            .catch((error) : void => {
                processError(error, onError);
            });
    }
}
