import BaseModel from "./BaseModel";
import KeyValue from "./KeyValue";
export default class Device extends BaseModel {
    IsActive: boolean;
    Name: string;
    EnvName: string;
    Description: string;
    Roles: string[];
    Preferences: KeyValue[];
    Settings: KeyValue[];
}
