import BaseModel from "./BaseModel";
import Vat from "./Vat";
export default class TicketItemPlu extends BaseModel {
    ArticleCategoryLabel: string;
    Code: number;
    Description: string;
    IsDiscountAllowed: boolean;
    IsPriceFixed: boolean;
    IsSplittable: boolean;
    Name: string;
    OrderEndpointNames: string[];
    StockName: string;
    Type: string;
    CourseNumber: number;
    VatInfo: Vat;
}
