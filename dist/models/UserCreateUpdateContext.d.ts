import BaseModel from "./BaseModel";
import UserDevice from "./UserDevice";
export default class UserCreateUpdateContext extends BaseModel {
    IsActive: boolean;
    Name: string;
    UserName: string;
    Password: string;
    ConfirmPassword: string;
    Devices: UserDevice[];
}
