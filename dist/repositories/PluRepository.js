"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var Plu_1 = require("../models/Plu");
var PluRepository = (function (_super) {
    __extends(PluRepository, _super);
    function PluRepository() {
        return _super.call(this, "/Plus", Plu_1.default) || this;
    }
    return PluRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PluRepository;
//# sourceMappingURL=PluRepository.js.map