import BaseModel from "./BaseModel";
import UserInfo from "./UserInfo";
import StockTakingItem from "./StockTakingItem";
export default class StockTaking extends BaseModel {
    Id: string;
    CreatedAt: Date;
    ClosedAt: Date;
    CreatedBy: UserInfo;
    ClosedBy: UserInfo;
    CurrencyLabel: string;
    Items: StockTakingItem[];
}
