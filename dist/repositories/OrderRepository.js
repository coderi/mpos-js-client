"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var Order_1 = require("../models/Order");
var OrderRepository = (function (_super) {
    __extends(OrderRepository, _super);
    function OrderRepository() {
        return _super.call(this, "/Orders", Order_1.default) || this;
    }
    OrderRepository.prototype.processOrders = function (ticketIds, onSuccess, onError) {
        var data = {
            Query: {
                TicketIds: ticketIds
            }
        };
        this.send("POST", "/OrderService.Process", data, function (data, response) {
            var orders = BaseRepository_1.dataToCollection(Order_1.default, data.value);
            onSuccess(orders, response);
        }, onError);
    };
    OrderRepository.prototype.setOrderStatus = function (id, status, onSuccess, onError) {
        var data = {
            Context: {
                Status: status
            }
        };
        this.sendToId(id, "POST", "/OrderService.SetOrderStatus", data, function (data, response) {
            var order = BaseRepository_1.dataToModel(Order_1.default, data);
            onSuccess(order, response);
        }, onError);
    };
    OrderRepository.prototype.setOrderItemStatus = function (id, status, orderItemId, onSuccess, onError) {
        var data = {
            Context: {
                OrderItemId: orderItemId,
                Status: status
            }
        };
        this.sendToId(id, "POST", "/OrderService.SetOrderItemStatus", data, function (data, response) {
            var order = BaseRepository_1.dataToModel(Order_1.default, data);
            onSuccess(order, response);
        }, onError);
    };
    OrderRepository.prototype.setOrderEndpointStatus = function (id, status, orderEndpointName, onSuccess, onError) {
        var data = {
            Context: {
                OrderEndpointName: orderEndpointName,
                Status: status
            }
        };
        this.sendToId(id, "POST", "/OrderService.SetOrderEndpointStatus", data, function (data, response) {
            var order = BaseRepository_1.dataToModel(Order_1.default, data);
            onSuccess(order, response);
        }, onError);
    };
    return OrderRepository;
}(BaseRepository_1.ROdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = OrderRepository;
//# sourceMappingURL=OrderRepository.js.map