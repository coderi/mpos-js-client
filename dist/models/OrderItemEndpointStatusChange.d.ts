import BaseModel from "./BaseModel";
export default class OrderItemEndpointStatusChange extends BaseModel {
    Status: string;
    Date: Date;
}
