import BaseModel from "./BaseModel";
import UserSessionProfile from "./UserSessionProfile";
export default class UserSession extends BaseModel {
    TokenId: string;
    User: UserSessionProfile;
}
