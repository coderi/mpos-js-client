import { CRUDOdataRepository } from "./BaseRepository";
import Device from "../models/Device";
export default class DeviceRepository extends CRUDOdataRepository<Device, string> {
    constructor();
}
