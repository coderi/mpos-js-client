import { CRUDOdataRepository } from "./BaseRepository";

import PaymentType from "../models/PaymentType";

export default class PaymentTypeRepository extends CRUDOdataRepository<PaymentType, number> { // Number
    constructor() {
        super("/PaymentTypes", PaymentType);
    }
}
