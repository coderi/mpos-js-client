"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var Localize_1 = require("../locales/Localize");
var BaseModel_1 = require("./BaseModel");
var TicketItemPlu_1 = require("./TicketItemPlu");
var Money_1 = require("./Money");
var Quantity_1 = require("./Quantity");
var TicketItem = (function (_super) {
    __extends(TicketItem, _super);
    function TicketItem() {
        var _this = _super.apply(this, arguments) || this;
        _this.DiscountRate = 0;
        return _this;
    }
    TicketItem.prototype.getNormalizedDiscountRate = function () {
        return this.DiscountRate / 100;
    };
    TicketItem.prototype.getNettoUnitPrice = function () {
        return this.UnitPriceBrutto.applyDiscount(this.getNormalizedDiscountRate());
    };
    TicketItem.prototype.getTotalBruttoPrice = function () {
        return this.UnitPriceBrutto.multiplyByNumber(this.Quantity.getRoundedAmount());
    };
    TicketItem.prototype.getTotalNettoPrice = function () {
        return this.getNettoUnitPrice().multiplyByNumber(this.Quantity.getRoundedAmount());
    };
    TicketItem.prototype.getDiscountAmount = function () {
        return this.getTotalBruttoPrice().subtract(this.getTotalNettoPrice());
    };
    return TicketItem;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsNumber()
], TicketItem.prototype, "Id", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], TicketItem.prototype, "Comment", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber(),
    class_validator_1.Min(0, {
        message: Localize_1.default("error.validation.min")
    }),
    class_validator_1.Max(100, {
        message: Localize_1.default("error.validation.max")
    })
], TicketItem.prototype, "DiscountRate", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return TicketItemPlu_1.default; })
], TicketItem.prototype, "Plu", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], TicketItem.prototype, "UnitPriceBrutto", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Quantity_1.default; })
], TicketItem.prototype, "Quantity", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TicketItem;
//# sourceMappingURL=TicketItem.js.map