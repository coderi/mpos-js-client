import { Type } from "class-transformer";

import { IsDefined, IsNotEmpty, IsString, IsBoolean, IsArray, Length, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import KeyValue from "./KeyValue";

export default class Fiscal extends BaseModel {
    @IsDefined()
    @IsBoolean()
    IsActive   : boolean;

    @IsString()
    @IsNotEmpty()
    Name       : string;

    @IsString()
    @IsNotEmpty()
    Type       : string;

    @IsDefined()
    @IsString()
    @Length(16)
    DKP        : string;

    @IsDefined()
    @IsString()
    Version    : string;

    @ValidateNested()
    @IsDefined()
    @IsArray()
    @Type(() => KeyValue)
    Settings   : KeyValue[];
}
