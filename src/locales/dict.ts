export default {
    "sk-SK": {
        "error.unknown.message"         : "Neznáma chyba",
        "error.404.message"             : "Zdroj sa nenašiel",
        "error.network.message"         : "Chyba spojenia so serverom",
        "error.typeerror.message"       : "Nepodarilo sa spracovat odpoveď zo servera",

        "error.invalidRequest.message"  : "Požiadavka má nesprávny formát",

        "error.invalidtype.message"     : "Nesprávny typ údajov",
        "error.invalid.message"         : "Nesprávne údaje",
        "error.invalidChildren.message" : "Nesprávne údaje",

        "error.validation.isString"     : "Pole musí obsahovať text",
        "error.validation.isBoolean"    : "Pole musí obsahovať logickú hodnotu",
        "error.validation.isNumber"     : "Pole musí obsahovať číselnú hodnotu",
        "error.validation.isArray"      : "Pole musí obsahovať zoznam",
        "error.validation.isDate"       : "Pole musí obsahovať dátum",
        "error.validation.isUrl"        : "Pole musí obsahovať URL odkaz",

        "error.validation.IsDefined"    : "Pole musí byť vyplnené",
        "error.validation.isHexColor"   : "Pole musí obsahovať farbu v RGB HEX formáte (napr.: #cccccc)",

        "error.validation.min"          : "Hodnota musí byť väčsia alebo rovná ako $constraint1",
        "error.validation.max"          : "Hodnota musí byť menšia alebo rovná ako $constraint1",
        "error.validation.length"       : "Dĺžka textu musí byť $constraint1",
        "error.validation.maxLength"    : "Dĺžka textu musí byť najviac $constraint1"
    }
};
