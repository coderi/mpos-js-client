import BaseModel from "./BaseModel";
import Money from "./Money";
export default class PluPriceHistory extends BaseModel {
    Date: Date;
    RetailPrice: Money;
}
