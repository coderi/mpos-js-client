import { CRUDOdataRepository } from "./BaseRepository";
import VatCategory from "../models/VatCategory";
export default class VatCategoryRepository extends CRUDOdataRepository<VatCategory, number> {
    constructor();
}
