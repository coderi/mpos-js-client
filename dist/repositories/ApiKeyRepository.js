"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var ApiKey_1 = require("../models/ApiKey");
var ApiKeyRepository = (function (_super) {
    __extends(ApiKeyRepository, _super);
    function ApiKeyRepository() {
        return _super.call(this, "/ApiKeys", ApiKey_1.default) || this;
    }
    return ApiKeyRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ApiKeyRepository;
//# sourceMappingURL=ApiKeyRepository.js.map