import { Type, plainToClass } from "class-transformer";

import { IsDefined, IsString, IsNotEmpty, IsArray, IsBoolean, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import UserDevice from "./UserDevice";
import UserInfo from "./UserInfo";

export default class User extends BaseModel {
    @IsDefined()
    @IsBoolean()
    IsActive    : boolean;

    @IsString()
    @IsNotEmpty()
    Name        : string;

    @IsString()
    @IsNotEmpty()
    UserName    : string;

    @IsDefined()
    @IsBoolean()
    HasPassword : boolean;

    @ValidateNested()
    @IsDefined()
    @IsArray()
    @Type(() => UserDevice)
    Devices     : UserDevice[];

    getInfo () : UserInfo {
        return plainToClass(UserInfo, {
            Name: this.Name,
            UserName: this.UserName
        } as UserInfo);
    }
}
