import BaseModel from "./BaseModel";
import StockTakingItemPlu from "./StockTakingItemPlu";
import Quantity from "./Quantity";
export default class StockTakingItem extends BaseModel {
    ActualQuantity: Quantity;
    ExpectedQuantity: Quantity;
    Plu: StockTakingItemPlu;
}
