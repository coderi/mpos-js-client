import { ExceptionCallback } from "../models/Exception";
export declare function dataToModel<U>(value: any, data: Object): U;
export declare function dataToCollection<U>(value: any, data: Object[]): U[];
export declare function sendRequest(method: string, uri: string, data: any, onSuccess: (data: any, response: any) => void, onError: ExceptionCallback): void;
export declare class RestRepository {
    private resourceUri;
    constructor(resourceUri: string);
    protected send(method: string, path: string, data: any, onSuccess: (data: any, response: any) => void, onError: ExceptionCallback): void;
}
export declare class OdataRepository<U> {
    protected resourceUri: string;
    protected model: any;
    constructor(resourceUri: string, model: any);
    protected send(method: string, path: string, data: any, onSuccess: (data: any, response: any) => void, onError: ExceptionCallback): void;
    protected sendToId(id: U, method: string, path: string, data: any, onSuccess: (data: any, response: any) => void, onError: ExceptionCallback): void;
}
export declare class ROdataRepository<T, U> extends OdataRepository<U> {
    findAll(onSuccess: (collection: T[], response: any) => void, onError: ExceptionCallback): void;
    findById(id: U, onSuccess: (model: T, response: any) => void, onError: ExceptionCallback): void;
    find(options: any, onSuccess: (collection: T[], response: any) => void, onError: ExceptionCallback): void;
}
export declare class RDOdataRepository<T, U> extends ROdataRepository<T, U> {
    delete(id: U, onSuccess: (model: T, response: any) => void, onError: ExceptionCallback): void;
}
export declare class RUDOdataRepository<T, U> extends RDOdataRepository<T, U> {
    update(id: U, model: T, onSuccess: (model: T, response: any) => void, onError: ExceptionCallback): void;
}
export declare class CRUDOdataRepository<T, U> extends RUDOdataRepository<T, U> {
    create(model: T, onSuccess: (model: T, response: any) => void, onError: ExceptionCallback): void;
}
