"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var Localize_1 = require("../locales/Localize");
var BaseModel_1 = require("./BaseModel");
var Vat_1 = require("./Vat");
var TicketItemPlu = (function (_super) {
    __extends(TicketItemPlu, _super);
    function TicketItemPlu() {
        return _super.apply(this, arguments) || this;
    }
    return TicketItemPlu;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsString(),
    class_validator_1.Length(3, null, {
        message: Localize_1.default("error.validation.length")
    })
], TicketItemPlu.prototype, "ArticleCategoryLabel", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], TicketItemPlu.prototype, "Code", void 0);
__decorate([
    class_validator_1.IsString()
], TicketItemPlu.prototype, "Description", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], TicketItemPlu.prototype, "IsDiscountAllowed", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], TicketItemPlu.prototype, "IsPriceFixed", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], TicketItemPlu.prototype, "IsSplittable", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty()
], TicketItemPlu.prototype, "Name", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsArray()
], TicketItemPlu.prototype, "OrderEndpointNames", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], TicketItemPlu.prototype, "StockName", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString(),
    class_validator_1.IsIn(["Unknown", "Recipe", "StockItem", "Credit", "Discount", "Service"])
], TicketItemPlu.prototype, "Type", void 0);
__decorate([
    class_validator_1.IsNumber()
], TicketItemPlu.prototype, "CourseNumber", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Vat_1.default; })
], TicketItemPlu.prototype, "VatInfo", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TicketItemPlu;
//# sourceMappingURL=TicketItemPlu.js.map