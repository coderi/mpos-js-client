import BaseModel from "./BaseModel";
import OrderItem from "./OrderItem";
import OrderTicketInfo from "./OrderTicketInfo";
import UserInfo from "./UserInfo";
export default class Order extends BaseModel {
    DeviceName: string;
    Id: string;
    Number: number;
    Items: OrderItem[];
    TicketInfo: OrderTicketInfo;
    UserInfo: UserInfo;
}
