import { CRUDOdataRepository } from "./BaseRepository";

import Role from "../models/Role";

export default class RoleRepository extends CRUDOdataRepository<Role, string> { // Name
    constructor() {
        super("/Roles", Role);
    }
}
