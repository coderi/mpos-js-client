import { Type, Transform } from "class-transformer";

import { IsDefined, IsString, IsNumber, IsBoolean, IsDate, IsNotEmpty, Min, Max, ValidateNested } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import Address from "./Address";
import Contact from "./Contact";
import Money from "./Money";

export default class Customer extends BaseModel {
    @IsDefined()
    @IsString()
    CardId         : string;

    @IsDefined()
    @IsNumber()
    CreditRate     : number;

    @IsString()
    CRN            : string;

    @IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    @Max(100, {
        message: t("error.validation.max")
    })
    DiscountRate   : number;

    @IsDefined()
    @IsBoolean()
    IsActive       : boolean;

    @IsString()
    @IsNotEmpty()
    Name           : string;

    @IsString()
    VatId          : string;

    @IsString()
    TaxId          : string;

    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    ExpirationDate : Date;

    @ValidateNested()
    @IsDefined()
    @Type(() => Address)
    Address        : Address;

    @ValidateNested()
    @IsDefined()
    @Type(() => Contact)
    Contact        : Contact;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    Credit         : Money;
}
