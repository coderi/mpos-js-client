import { CRUDOdataRepository } from "./BaseRepository";
import { ExceptionCallback } from "../models/Exception";
import UserInfo from "../models/UserInfo";
import Ticket from "../models/Ticket";
export default class TicketRepository extends CRUDOdataRepository<Ticket, string> {
    constructor();
    closeTicket(id: string, employee: UserInfo, fiscalName: string, onSuccess: (ticket: Ticket, response: any) => void, onError: ExceptionCallback): void;
    printPreliminaryTicket(id: string, orderEndpointName: string, onSuccess: (ticket: Ticket, response: any) => void, onError: ExceptionCallback): void;
    updateTickets(tickets: Ticket[], action: string, onSuccess: (tickets: Ticket[], response: any) => void, onError: ExceptionCallback): void;
}
