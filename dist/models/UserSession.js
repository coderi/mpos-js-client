"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var BaseModel_1 = require("./BaseModel");
var UserSessionProfile_1 = require("./UserSessionProfile");
var UserSession = (function (_super) {
    __extends(UserSession, _super);
    function UserSession() {
        return _super.apply(this, arguments) || this;
    }
    return UserSession;
}(BaseModel_1.default));
__decorate([
    class_transformer_1.Type(function () { return UserSessionProfile_1.default; })
], UserSession.prototype, "User", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = UserSession;
//# sourceMappingURL=UserSession.js.map