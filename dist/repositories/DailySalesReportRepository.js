"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var DailySalesReport_1 = require("../models/DailySalesReport");
var DailySalesReportRepository = (function (_super) {
    __extends(DailySalesReportRepository, _super);
    function DailySalesReportRepository() {
        return _super.call(this, "/DailySalesReports", DailySalesReport_1.default) || this;
    }
    return DailySalesReportRepository;
}(BaseRepository_1.RUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = DailySalesReportRepository;
//# sourceMappingURL=DailySalesReportRepository.js.map