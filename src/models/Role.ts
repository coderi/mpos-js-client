import { IsDefined, IsNotEmpty, IsString, IsArray } from "class-validator";

import BaseModel from "./BaseModel";

export default class Role extends BaseModel {
    @IsString()
    @IsNotEmpty()
    Name           : string;

    @IsDefined()
    @IsString()
    Label          : string;

    @IsString()
    Description    : string;

    @IsDefined()
    @IsArray()
    Rights         : string[];
}
