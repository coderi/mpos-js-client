import BaseModel from "./BaseModel";
export default class Quantity extends BaseModel {
    Amount: number;
    Unit: string;
    getRoundedAmount(): number;
}
