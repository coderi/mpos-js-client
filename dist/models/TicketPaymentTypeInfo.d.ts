import BaseModel from "./BaseModel";
export default class TicketPaymentTypeInfo extends BaseModel {
    Number: number;
    Description: string;
    IsChangeable: boolean;
}
