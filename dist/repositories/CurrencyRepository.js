"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var Currency_1 = require("../models/Currency");
var CurrencyRepository = (function (_super) {
    __extends(CurrencyRepository, _super);
    function CurrencyRepository() {
        return _super.call(this, "/Currencies", Currency_1.default) || this;
    }
    return CurrencyRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = CurrencyRepository;
//# sourceMappingURL=CurrencyRepository.js.map