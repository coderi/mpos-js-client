import { CRUDOdataRepository } from "./BaseRepository";
import OrderEndpoint from "../models/OrderEndpoint";
export default class OrderEndpointRepository extends CRUDOdataRepository<OrderEndpoint, string> {
    constructor();
}
