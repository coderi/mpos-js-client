import { Type, Transform } from "class-transformer";

import { IsDefined, IsDate, IsIn, IsString } from "class-validator";

import BaseModel from "./BaseModel";

export default class OrderItemEndpointStatusChange extends BaseModel {
    @IsDefined()
    @IsString()
    @IsIn(["Unkonwn", "Created", "Sent", "Delivered", "Refused", "Failed", "Accepted", "ProcessSkipped", "Processed"])
    Status  : string;

    @IsDefined()
    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    Date    : Date;
}
