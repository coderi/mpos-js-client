"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var class_transformer_1 = require("class-transformer");
var BaseRepository_1 = require("./BaseRepository");
var Fiscal_1 = require("../models/Fiscal");
var DailySalesReport_1 = require("../models/DailySalesReport");
var FiscalRepository = (function (_super) {
    __extends(FiscalRepository, _super);
    function FiscalRepository() {
        return _super.call(this, "/Fiscals", Fiscal_1.default) || this;
    }
    FiscalRepository.prototype.openDrawer = function (id, onSuccess, onError) {
        this.sendToId(id, "POST", "/FiscalService.OpenDrawer", {}, function (data, response) {
            onSuccess(response);
        }, onError);
    };
    FiscalRepository.prototype.createCashTransfer = function (id, cashTranfser, onSuccess, onError) {
        var data = {
            CashTransfer: class_transformer_1.classToPlain(cashTranfser)
        };
        this.sendToId(id, "POST", "/FiscalService.CashTransfer", data, function (data, response) {
            var dsr = BaseRepository_1.dataToModel(DailySalesReport_1.default, data);
            onSuccess(dsr, response);
        }, onError);
    };
    FiscalRepository.prototype.printRecordCopy = function (id, dailySalesReportNumber, dailySalesReportDate, ticketNumber, onSuccess, onError) {
        var data = {
            Context: {
                DailySalesReportNumber: dailySalesReportNumber,
                DailySalesReportDate: dailySalesReportDate,
                TicketNumber: ticketNumber
            }
        };
        this.sendToId(id, "POST", "/FiscalService.PrintRecordCopy", data, function (data, response) {
            onSuccess(response);
        }, onError);
    };
    FiscalRepository.prototype.printLastRecordCopy = function (id, onSuccess, onError) {
        this.sendToId(id, "POST", "/FiscalService.PrintLastRecordCopy", {}, function (data, response) {
            onSuccess(response);
        }, onError);
    };
    FiscalRepository.prototype.printOverviewSalesReport = function (id, onSuccess, onError) {
        this.sendToId(id, "POST", "/FiscalService.PrintOverviewSalesReport", {}, function (data, response) {
            onSuccess(response);
        }, onError);
    };
    FiscalRepository.prototype.printDailySalesReport = function (id, onSuccess, onError) {
        this.sendToId(id, "POST", "/FiscalService.DoDailySalesReport", {}, function (data, response) {
            var dsr = BaseRepository_1.dataToModel(DailySalesReport_1.default, data);
            onSuccess(dsr, response);
        }, onError);
    };
    FiscalRepository.prototype.printDailySalesReportCopy = function (id, dailySalesReportNumber, dailySalesReportDate, onSuccess, onError) {
        var data = {
            Context: {
                DailySalesReportNumber: dailySalesReportNumber,
                DailySalesReportDate: dailySalesReportDate
            }
        };
        this.sendToId(id, "POST", "/FiscalService.PrintDailySalesReportCopy", data, function (data, response) {
            onSuccess(response);
        }, onError);
    };
    FiscalRepository.prototype.printLastDailySalesReportCopy = function (id, onSuccess, onError) {
        this.sendToId(id, "POST", "/FiscalService.PrintLastDailySalesReportCopy", {}, function (data, response) {
            onSuccess(response);
        }, onError);
    };
    FiscalRepository.prototype.printSummaryIntervalSalesReport = function (id, dsrDateFrom, dsrDateTo, dsrNumberFrom, dsrNumberTo, onSuccess, onError) {
        var data = {
            Context: {
                DsrDateFrom: dsrDateFrom,
                DsrDateTo: dsrDateTo,
                DsrNumberFrom: dsrNumberFrom,
                DsrNumberTo: dsrNumberTo
            }
        };
        this.sendToId(id, "POST", "/FiscalService.PrintSummaryIntervalSalesReport", data, function (data, response) {
            onSuccess(response);
        }, onError);
    };
    FiscalRepository.prototype.printDetailedIntervalSalesReport = function (id, dsrDateFrom, dsrDateTo, dsrNumberFrom, dsrNumberTo, onSuccess, onError) {
        var data = {
            Context: {
                DsrDateFrom: dsrDateFrom,
                DsrDateTo: dsrDateTo,
                DsrNumberFrom: dsrNumberFrom,
                DsrNumberTo: dsrNumberTo
            }
        };
        this.sendToId(id, "POST", "/FiscalService.PrintDetailedIntervalSalesReport", data, function (data, response) {
            onSuccess(response);
        }, onError);
    };
    FiscalRepository.prototype.printNonfiscalRecord = function (id, body, onSuccess, onError) {
        var data = {
            Context: {
                RecordBody: body
            }
        };
        this.sendToId(id, "POST", "/FiscalService.PrintNonfiscalRecord", data, function (data, response) {
            onSuccess(response);
        }, onError);
    };
    return FiscalRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = FiscalRepository;
//# sourceMappingURL=FiscalRepository.js.map