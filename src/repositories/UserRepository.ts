import { classToPlain } from "class-transformer";

import { RDOdataRepository, dataToModel } from "./BaseRepository";

import { ExceptionCallback } from "../models/Exception";
import User from "../models/User";
import UserCreateUpdateContext from "../models/UserCreateUpdateContext";

export default class UserRepository extends RDOdataRepository<User, string> { // UserName
    constructor() {
        super("/Users", User);
    }

    create (
        model: UserCreateUpdateContext,
        onSuccess: (model: User, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = classToPlain(model);

        this.send("POST", "", data,
            (data, response) : void => {
                let user = dataToModel<User>(User, data.User);

                onSuccess(user, response);
            },
            onError
        );
    }
    update (
        id: string,
        model: UserCreateUpdateContext,
        onSuccess: (model: User, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = classToPlain(model);

        this.sendToId(id, "PUT", "", data,
            (data, response) : void => {
                let user = dataToModel<User>(User, data.User);

                onSuccess(user, response);
            },
            onError
        );
    }
}
