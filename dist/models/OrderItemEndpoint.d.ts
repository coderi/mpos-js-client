import BaseModel from "./BaseModel";
import OrderItemEndpointStatusChange from "./OrderItemEndpointStatusChange";
export default class OrderItemEndpoint extends BaseModel {
    Name: string;
    StatusChanges: OrderItemEndpointStatusChange[];
}
