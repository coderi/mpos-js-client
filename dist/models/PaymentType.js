"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_validator_1 = require("class-validator");
var Localize_1 = require("../locales/Localize");
var BaseModel_1 = require("./BaseModel");
var PaymentType = (function (_super) {
    __extends(PaymentType, _super);
    function PaymentType() {
        return _super.apply(this, arguments) || this;
    }
    return PaymentType;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString(),
    class_validator_1.IsIn(["Cash", "CreditCard", "Other"])
], PaymentType.prototype, "Category", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber(),
    class_validator_1.Min(1, {
        message: Localize_1.default("error.validation.min")
    }),
    class_validator_1.Max(10, {
        message: Localize_1.default("error.validation.max")
    })
], PaymentType.prototype, "Number", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], PaymentType.prototype, "IsActive", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], PaymentType.prototype, "Description", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], PaymentType.prototype, "IsChangeable", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PaymentType;
//# sourceMappingURL=PaymentType.js.map