import BaseModel from "./BaseModel";
import TicketItemPlu from "./TicketItemPlu";
import Money from "./Money";
import Quantity from "./Quantity";
export default class TicketItem extends BaseModel {
    Id: number;
    Comment: string;
    DiscountRate: number;
    Plu: TicketItemPlu;
    UnitPriceBrutto: Money;
    Quantity: Quantity;
    getNormalizedDiscountRate(): number;
    getNettoUnitPrice(): Money;
    getTotalBruttoPrice(): Money;
    getTotalNettoPrice(): Money;
    getDiscountAmount(): Money;
}
