import { CRUDOdataRepository } from "./BaseRepository";

import Currency from "../models/Currency";

export default class CurrencyRepository extends CRUDOdataRepository<Currency, string> { // Label
    constructor() {
        super("/Currencies", Currency);
    }
}
