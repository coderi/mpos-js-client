import { IsDefined, IsString, IsNumber, IsBoolean, IsIn, Min, Max } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

export default class PaymentType extends BaseModel {

    @IsDefined()
    @IsString()
    @IsIn(["Cash", "CreditCard", "Other"])
    Category        : string;

    @IsDefined()
    @IsNumber()
    @Min(1, {
        message: t("error.validation.min")
    })
    @Max(10, {
        message: t("error.validation.max")
    })
    Number          : number;

    @IsDefined()
    @IsBoolean()
    IsActive        : boolean;

    @IsDefined()
    @IsString()
    Description     : string;

    @IsDefined()
    @IsBoolean()
    IsChangeable    : boolean;
}
