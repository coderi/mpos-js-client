import { CRUDOdataRepository } from "./BaseRepository";

import Company from "../models/Company";

export default class CompanyRepository extends CRUDOdataRepository<Company, string> { // Id
    constructor() {
        super("/Companies", Company);
    }
}
