import BaseModel from "./BaseModel";
export default class PaymentType extends BaseModel {
    Category: string;
    Number: number;
    IsActive: boolean;
    Description: string;
    IsChangeable: boolean;
}
