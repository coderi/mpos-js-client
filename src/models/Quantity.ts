import { IsDefined, IsString, IsNumber, MaxLength } from "class-validator";

import BaseModel from "./BaseModel";

import t from "../locales/Localize";

export default class Quantity extends BaseModel {
    @IsDefined()
    @IsNumber()
    Amount  : number = 0;

    @IsDefined()
    @IsString()
    @MaxLength(3, {
        message: t("error.validation.maxLength")
    })
    Unit    : string;

    getRoundedAmount () : number {
        return Math.round(this.Amount * 1000) / 1000;
    }
}
