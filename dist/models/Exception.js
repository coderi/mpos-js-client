"use strict";
var ExceptionCodes;
(function (ExceptionCodes) {
    ExceptionCodes[ExceptionCodes["Unspecified"] = 0] = "Unspecified";
    ExceptionCodes[ExceptionCodes["Unauthenticated"] = 1] = "Unauthenticated";
    ExceptionCodes[ExceptionCodes["Unauthorized"] = 2] = "Unauthorized";
    ExceptionCodes[ExceptionCodes["ValidationFailed"] = 3] = "ValidationFailed";
    ExceptionCodes[ExceptionCodes["LicenceExpired"] = 4] = "LicenceExpired";
    ExceptionCodes[ExceptionCodes["ResourceLocked"] = 5] = "ResourceLocked";
})(ExceptionCodes = exports.ExceptionCodes || (exports.ExceptionCodes = {}));
var Exception = (function () {
    function Exception() {
    }
    return Exception;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Exception;
var ValidationException = (function () {
    function ValidationException() {
    }
    return ValidationException;
}());
exports.ValidationException = ValidationException;
//# sourceMappingURL=Exception.js.map