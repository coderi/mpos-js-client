import axios from "axios";

import { Localize } from "./locales/Localize";

export default class MPos {
    static initialize (apiUrl: string) : void {
        axios.defaults.baseURL = apiUrl;

        axios.defaults.headers.common["Content-Type"] = "application/json; charset=utf-8";
        axios.defaults.headers.common["Accept"] = "application/json; charset=utf-8";
    }
    static setAccessToken (accessToken: string) : void {
        axios.defaults.headers.common["X-Access-Token"] = accessToken;
    }
    static removeAccessToken () : void {
        delete axios.defaults.headers.common["X-Access-Token"];
    }
    static setLanguage (language: string) : void {
        axios.defaults.headers.common["Accept-Language"] = language;

        Localize.setLocale(language);
    }
    static removeLanguage () : void {
        delete axios.defaults.headers.common["Accept-Language"];
    }
}
