import dict from "./dict";

export class Localize {
    public static locale : string = "sk-SK";

    static setLocale (locale: string) {
        Localize.locale = locale;
    }
}

export default function translate (key: string, params?: any[]) : string {
    let message = (dict.hasOwnProperty(Localize.locale) && dict[Localize.locale].hasOwnProperty(key) ? dict[Localize.locale][key] : key);

    if (params && params.length > 0) {
        for (var i = 0; i < params.length; i++) {
            message = message.replace("{"+i+"}", params[i]);
        }
    }

    return message;
}
