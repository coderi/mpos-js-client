import BaseModel from "./BaseModel";
export default class Stock extends BaseModel {
    Name: string;
    Description: string;
}
