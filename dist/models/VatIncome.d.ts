import BaseModel from "./BaseModel";
import Money from "./Money";
export default class VatIncome extends BaseModel {
    VatCategory: number;
    VatRate: number;
    TotalAmount: Money;
    VatAmount: Money;
}
