import { Type } from "class-transformer";

import { IsDefined, IsString, IsArray, IsNotEmpty, IsBoolean, IsIn, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import KeyValue from "./KeyValue";

export default class UserDevice extends BaseModel {
    @IsDefined()
    @IsBoolean()
    IsActive    : boolean;

    @IsString()
    @IsNotEmpty()
    Name        : string;

    @IsDefined()
    @IsString()
    @IsIn(["", "CashRegister", "PDA", "BackOffice", "Tool"])
    EnvName     : string;

    @IsDefined()
    @IsString()
    Description : string;

    @IsDefined()
    @IsArray()
    Roles       : string[];

    @ValidateNested()
    @IsDefined()
    @IsArray()
    @Type(() => KeyValue)
    Preferences : KeyValue[];
}
