"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var UserSession_1 = require("../models/UserSession");
var UserSessionProfile_1 = require("../models/UserSessionProfile");
var AuthRepository = (function (_super) {
    __extends(AuthRepository, _super);
    function AuthRepository() {
        return _super.call(this, "/auth") || this;
    }
    AuthRepository.prototype.login = function (username, password, deviceName, onSuccess, onError) {
        this.send("POST", "", {
            UserName: username,
            Password: password,
            DeviceName: deviceName
        }, function (data, response) {
            var session = BaseRepository_1.dataToModel(UserSession_1.default, data);
            onSuccess(session, response);
        }, onError);
    };
    AuthRepository.prototype.logout = function (onSuccess, onError) {
        this.send("DELETE", "", {}, function (data, response) {
            onSuccess(response);
        }, onError);
    };
    AuthRepository.prototype.loadCurrentUser = function (onSuccess, onError) {
        this.send("GET", "", {}, function (data, response) {
            var user = BaseRepository_1.dataToModel(UserSessionProfile_1.default, data);
            onSuccess(user, response);
        }, onError);
    };
    return AuthRepository;
}(BaseRepository_1.RestRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = AuthRepository;
//# sourceMappingURL=AuthRepository.js.map