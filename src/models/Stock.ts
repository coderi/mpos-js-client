import { IsDefined, IsNotEmpty, IsString } from "class-validator";

import BaseModel from "./BaseModel";

export default class Stock extends BaseModel {
    @IsString()
    @IsNotEmpty()
    Name        : string;

    @IsString()
    Description : string;
}
