require("./init");
require("./autologin");

/*
 *  Dependencies
 */
var MPosLib = require("../dist/index"); // In production: require("mpos-lib")

var AuthRepository = MPosLib.AuthRepository;

/*
 *  Authentication
 */
var authRepository = new AuthRepository();

// Login
/*
authRepository.login(
    "999",          // Username
    "",             // Password
    "BackOffice",   // DeviceName
    function (session, response) {
        console.log("Success");
        console.log(session);
    },
    function (error, response) {
        console.log("Error");
        console.log(error);
    }
);
*/
// Load Current User

authRepository.loadCurrentUser(
    function (user, response) {
        console.log("Success");
        console.log(user);
    },
    function (error, response) {
        console.log("Error");
        console.log(error);
    }
);


// Logout
/*
authRepository.logout(
    function (response) {
        console.log("Success");
    },
    function (error, response) {
        console.log("Error");
        console.log(error);
    }
);
*/
