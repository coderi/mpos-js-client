import { Type, Transform } from "class-transformer";

import { IsString, IsNumber, IsDefined, IsDate, IsArray, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import Money from "./Money";
import VatIncome from "./VatIncome";

export default class DailySalesReportFiscalInfo extends BaseModel {
    @IsDefined()
    @IsNumber()
    TicketsCount : number;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    StornoAmount : Money;

    @IsDefined()
    @IsNumber()
    StornoCount : number;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    DiscountAmount : Money;

    @IsDefined()
    @IsNumber()
    DiscountCount : number;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    NegativeAmount : Money;

    @IsDefined()
    @IsNumber()
    NegativeCount : number;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    ReturnAmount : Money;

    @IsDefined()
    @IsNumber()
    ReturnCount : number;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    InvalidAmount : Money;

    @IsDefined()
    @IsNumber()
    InvalidCount : number;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    RoundingUp : Money;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    RoundingDown : Money;

    @IsDefined()
    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    CloseDate : Date;

    @IsDefined()
    @Type(() => VatIncome)
    VatIncomes : VatIncome[];

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    GrandTotal : Money;
}
