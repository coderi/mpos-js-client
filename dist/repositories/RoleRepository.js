"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var Role_1 = require("../models/Role");
var RoleRepository = (function (_super) {
    __extends(RoleRepository, _super);
    function RoleRepository() {
        return _super.call(this, "/Roles", Role_1.default) || this;
    }
    return RoleRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = RoleRepository;
//# sourceMappingURL=RoleRepository.js.map