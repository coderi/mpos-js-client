import { CRUDOdataRepository } from "./BaseRepository";
import Customer from "../models/Customer";
export default class CustomerRepository extends CRUDOdataRepository<Customer, string> {
    constructor();
}
