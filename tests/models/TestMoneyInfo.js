var expect = require("chai").expect;

var mPosLib = require("../../dist/index.js");

var Money = mPosLib.Money;
var MoneyInfo = mPosLib.MoneyInfo;

describe("MoneyInfo", function() {
    describe("instantiation", function() {
        var value = new MoneyInfo();

        it("should have Amount set to 0", function() {
            expect(value.Amount).to.equal(0);
        });
    });

    describe("toDomestic() called on domestic money info", function() {
        var value = new MoneyInfo();
        value.CurrencyLabel = "EUR";
        value.DomesticCurrencyLabel = "EUR";
        value.ExchangeRate = 1;
        value.Amount = 123.45;

        var result = value.toDomestic();

        it("should return object", function() {
            expect(result).to.be.an('object');
        });
        it("should return Amount of type number", function() {
            expect(result.Amount).to.be.an('number');
        });
        it("should return ExchangeRate of type number", function() {
            expect(result.ExchangeRate).to.be.an('number');
        });
        it("should return CurrencyLabel of type string", function() {
            expect(result.CurrencyLabel).to.be.a('string');
        });
        it("should return DomesticCurrencyLabel of type string", function() {
            expect(result.DomesticCurrencyLabel).to.be.a('string');
        });
        it("should result to correct amount", function() {
            expect(result.Amount).to.equal(123.45);
        });
        it("should result to correct exchange rate", function() {
            expect(result.ExchangeRate).to.equal(1);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
        it("should result to correct domestic currency label", function() {
            expect(result.DomesticCurrencyLabel).to.equal("EUR");
        });
    });

    describe("toDomestic() called on non-domestic money info", function() {
        var value = new MoneyInfo();
        value.CurrencyLabel = "USD";
        value.DomesticCurrencyLabel = "EUR";
        value.ExchangeRate = 1.2369;
        value.Amount = 1000;

        var result = value.toDomestic();

        it("should return object", function() {
            expect(result).to.be.an('object');
        });
        it("should return Amount of type number", function() {
            expect(result.Amount).to.be.an('number');
        });
        it("should return ExchangeRate of type number", function() {
            expect(result.ExchangeRate).to.be.an('number');
        });
        it("should return CurrencyLabel of type string", function() {
            expect(result.CurrencyLabel).to.be.a('string');
        });
        it("should return DomesticCurrencyLabel of type string", function() {
            expect(result.DomesticCurrencyLabel).to.be.a('string');
        });
        it("should result to correct amount", function() {
            expect(result.Amount).to.equal(808.47);
        });
        it("should result to correct exchange rate", function() {
            expect(result.ExchangeRate).to.equal(1);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
        it("should result to correct domestic currency label", function() {
            expect(result.DomesticCurrencyLabel).to.equal("EUR");
        });
    });


    describe("toDomesticMoney() called on domestic money info", function() {
        var value = new MoneyInfo();
        value.CurrencyLabel = "EUR";
        value.DomesticCurrencyLabel = "EUR";
        value.ExchangeRate = 1;
        value.Amount = 123.45;

        var result = value.toDomesticMoney();

        it("should return object", function() {
            expect(result).to.be.an('object');
        });
        it("should return Amount of type number", function() {
            expect(result.Amount).to.be.an('number');
        });
        it("should not have ExchangeRate property", function() {
            expect(result.ExchangeRate).to.be.undefined;
        });
        it("should return CurrencyLabel of type string", function() {
            expect(result.CurrencyLabel).to.be.a('string');
        });
        it("should not have DomesticCurrencyLabel property", function() {
            expect(result.DomesticCurrencyLabel).to.be.undefined;
        });
        it("should result to correct amount", function() {
            expect(result.Amount).to.equal(123.45);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });

    describe("toDomesticMoney() called on non-domestic money info", function() {
        var value = new MoneyInfo();
        value.CurrencyLabel = "USD";
        value.DomesticCurrencyLabel = "EUR";
        value.ExchangeRate = 1.2369;
        value.Amount = 1000;

        var result = value.toDomesticMoney();

        it("should return object", function() {
            expect(result).to.be.an('object');
        });
        it("should return Amount of type number", function() {
            expect(result.Amount).to.be.an('number');
        });
        it("should not have ExchangeRate property", function() {
            expect(result.ExchangeRate).to.be.undefined;
        });
        it("should return CurrencyLabel of type string", function() {
            expect(result.CurrencyLabel).to.be.a('string');
        });
        it("should not have DomesticCurrencyLabel property", function() {
            expect(result.DomesticCurrencyLabel).to.be.undefined;
        });
        it("should result to correct amount", function() {
            expect(result.Amount).to.equal(808.47);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });

});