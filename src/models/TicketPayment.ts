import { Type } from "class-transformer";

import { IsDefined, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import MoneyInfo from "./MoneyInfo";
import TicketPaymentTypeInfo from "./TicketPaymentTypeInfo";

export default class TicketPayment extends BaseModel {
    @ValidateNested()
    @IsDefined()
    @Type(() => MoneyInfo)
    Amount      : MoneyInfo;

    @ValidateNested()
    @IsDefined()
    @Type(() => TicketPaymentTypeInfo)
    PaymentType : TicketPaymentTypeInfo;
}
