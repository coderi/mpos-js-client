import BaseModel from "./BaseModel";
import Money from "./Money";
import VatIncome from "./VatIncome";
export default class DailySalesReportFiscalInfo extends BaseModel {
    TicketsCount: number;
    StornoAmount: Money;
    StornoCount: number;
    DiscountAmount: Money;
    DiscountCount: number;
    NegativeAmount: Money;
    NegativeCount: number;
    ReturnAmount: Money;
    ReturnCount: number;
    InvalidAmount: Money;
    InvalidCount: number;
    RoundingUp: Money;
    RoundingDown: Money;
    CloseDate: Date;
    VatIncomes: VatIncome[];
    GrandTotal: Money;
}
