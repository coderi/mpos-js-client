import { Type } from "class-transformer";

import { IsDefined, IsString, IsArray, IsBoolean, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import UserDevice from "./UserDevice";

export default class UserCreateUpdateContext extends BaseModel {
    @IsDefined()
    @IsBoolean()
    IsActive    : boolean;

    @IsDefined()
    @IsString()
    Name        : string;

    @IsDefined()
    @IsString()
    UserName    : string;

    @IsDefined()
    @IsString()
    Password        : string;

    @IsDefined()
    @IsString()
    ConfirmPassword : string;

    @ValidateNested()
    @IsDefined()
    @IsArray()
    @Type(() => UserDevice)
    Devices     : UserDevice[];
}
