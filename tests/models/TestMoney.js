var expect = require("chai").expect;

var mPosLib = require("../../dist/index.js");

var Money = mPosLib.Money;

describe("Money", function() {
    describe("instantiation", function() {
        var value = new Money();

        it("should have Amount set to 0", function() {
            expect(value.Amount).to.equal(0);
        });
    });
    describe("rounding amount equal to zero", function() {
        var m1 = new Money();
        
        var result = m1.roundAmount(0);

        it("should result to 0", function() {
            expect(result).to.equal(0);
        });
    });
    describe("rounding amount with two decimal places", function() {
        var m1 = new Money();

        var result = m1.roundAmount(1.23);

        it("should result to same value", function() {
            expect(result).to.equal(1.23);
        });
    });
    describe("rounding amount 1.234", function() {
        var m1 = new Money();

        var result = m1.roundAmount(1.234);

        it("should result to value 1.23", function() {
            expect(result).to.equal(1.23);
        });
    });
    describe("rounding amount 1.235", function() {
        var m1 = new Money();

        var result = m1.roundAmount(1.235);

        it("should result to value 1.24", function() {
            expect(result).to.equal(1.24);
        });
    });
    describe("rounding amount 0.145", function() {
        var m1 = new Money();

        var result = m1.roundAmount(0.145);

        it("should result to value 0.15", function() {
            expect(result).to.equal(0.15);
        });
    });
    describe("addition of two default values", function() {
        var m1 = new Money();
        m1.CurrencyLabel = "EUR";
        var m2 = new Money();
        m2.CurrencyLabel = "EUR";
        
        var result = m1.add(m2);

        it("should result to 0 amount", function() {
            expect(result.Amount).to.equal(0);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });
    describe("addition of two non-zero values", function() {
        var m1 = new Money();
        m1.Amount = 1.23;
        m1.CurrencyLabel = "EUR";
        var m2 = new Money();
        m2.Amount = 4.56;
        m2.CurrencyLabel = "EUR";
        
        var result = m1.add(m2);

        it("should result to correct amount", function() {
            expect(result.Amount).to.equal(5.79);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });
    describe("subtraction of two default values", function() {
        var m1 = new Money();
        m1.CurrencyLabel = "EUR";
        var m2 = new Money();
        m2.CurrencyLabel = "EUR";
        
        var result = m1.subtract(m2);

        it("should result to 0 amount", function() {
            expect(result.Amount).to.equal(0);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });
    describe("subtraction of two same values", function() {
        var m1 = new Money();
        m1.Amount = 1.23;
        m1.CurrencyLabel = "EUR";
        var m2 = new Money();
        m2.Amount = 1.23;
        m2.CurrencyLabel = "EUR";
        
        var result = m1.subtract(m2);

        it("should result to 0 amount", function() {
            expect(result.Amount).to.equal(0);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });
    describe("subtraction of two different values", function() {
        var m1 = new Money();
        m1.Amount = 4.56;
        m1.CurrencyLabel = "EUR";
        var m2 = new Money();
        m2.Amount = 1.23;
        m2.CurrencyLabel = "EUR";
        
        var result = m1.subtract(m2);

        it("should result to correct amount", function() {
            expect(result.Amount).to.equal(3.33);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });
    describe("multiplication of two default values", function() {
        var m1 = new Money();
        m1.Amount = 0;
        m1.CurrencyLabel = "EUR";
        var m2 = new Money();
        m2.Amount = 0;
        m2.CurrencyLabel = "EUR";
        
        var result = m1.multiply(m2);

        it("should return object", function() {
            expect(result).to.be.an('object');
        });
        it("should return Amount of type number", function() {
            expect(result.Amount).to.be.an('number');
        });
        it("should return CurrencyLabel of type string", function() {
            expect(result.CurrencyLabel).to.be.a('string');
        });
        it("should result to 0 amount", function() {
            expect(result.Amount).to.equal(0);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });
    describe("multiplication of two non-zero values", function() {
        var m1 = new Money();
        m1.Amount = 1.23;
        m1.CurrencyLabel = "EUR";
        var m2 = new Money();
        m2.Amount = 4.56;
        m2.CurrencyLabel = "EUR";
        
        var result = m1.multiply(m2);

        it("should result to correct amount", function() {
            expect(result.Amount).to.equal(5.61);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });
    describe("divison of two non-zero values", function() {
        var m1 = new Money();
        m1.Amount = 123.45;
        m1.CurrencyLabel = "EUR";
        var m2 = new Money();
        m2.Amount = 6.78;
        m2.CurrencyLabel = "EUR";
        
        var result = m1.divide(m2);

        it("should return object", function() {
            expect(result).to.be.an('object');
        });
        it("should return Amount of type number", function() {
            expect(result.Amount).to.be.an('number');
        });
        it("should return CurrencyLabel of type string", function() {
            expect(result.CurrencyLabel).to.be.a('string');
        });
        it("should result to correct amount", function() {
            expect(result.Amount).to.equal(18.21);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });
    describe("applying discount to small value", function() {
        var value = new Money();
        value.Amount = 0.15;
        value.CurrencyLabel = "EUR";
        
        var result = value.applyDiscount(0.5);

        it("should return object", function() {
            expect(result).to.be.an('object');
        });
        it("should return Amount of type number", function() {
            expect(result.Amount).to.be.an('number');
        });
        it("should return CurrencyLabel of type string", function() {
            expect(result.CurrencyLabel).to.be.a('string');
        });
        it("should result to correct amount", function() {
            expect(result.Amount).to.equal(0.07);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });
    describe("applying discount with rounding", function() {
        var value = new Money();
        value.Amount = 2.90;
        value.CurrencyLabel = "EUR";

        var result = value.applyDiscount(0.05);

        it("should return object", function() {
            expect(result).to.be.an('object');
        });
        it("should return Amount of type number", function() {
            expect(result.Amount).to.be.an('number');
        });
        it("should return CurrencyLabel of type string", function() {
            expect(result.CurrencyLabel).to.be.a('string');
        });
        it("should result to correct amount", function() {
            expect(result.Amount).to.equal(2.75);
        });
        it("should result to correct currency label", function() {
            expect(result.CurrencyLabel).to.equal("EUR");
        });
    });
});