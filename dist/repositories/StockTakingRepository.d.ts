import { CRUDOdataRepository } from "./BaseRepository";
import StockTaking from "../models/StockTaking";
export default class StockTakingRepository extends CRUDOdataRepository<StockTaking, string> {
    constructor();
}
