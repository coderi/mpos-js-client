import BaseModel from "./BaseModel";
export default class Currency extends BaseModel {
    IsDomestic: boolean;
    Label: string;
    Sign: string;
    ExchangeRate: number;
    Description: string;
}
