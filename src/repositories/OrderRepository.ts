import { plainToClass, classToPlain } from "class-transformer";

import { ROdataRepository, dataToModel, dataToCollection } from "./BaseRepository";

import Exception, { ExceptionCallback } from "../models/Exception";
import Order from "../models/Order";

export default class OrderRepository extends ROdataRepository<Order, string> { // Id
    constructor () {
        super ("/Orders", Order);
    }

    processOrders (
        ticketIds: string[],
        onSuccess: (orders: Order[], response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Query: {
                TicketIds: ticketIds
            }
        };

        this.send("POST", "/OrderService.Process", data,
            (data, response) : void => {
                let orders = dataToCollection<Order>(Order, data.value);

                onSuccess(orders, response);
            },
            onError
        );
    }
    setOrderStatus (
        id: string,
        status: string,
        onSuccess: (order: Order, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Context: {
                Status: status
            }
        };

        this.sendToId(id, "POST", "/OrderService.SetOrderStatus", data,
            (data, response) : void => {
                let order = dataToModel<Order>(Order, data);

                onSuccess(order, response);
            },
            onError
        );
    }
    setOrderItemStatus (
        id: string,
        status: string,
        orderItemId: number,
        onSuccess: (order: Order, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Context: {
                OrderItemId: orderItemId,
                Status:      status
            }
        };

        this.sendToId(id, "POST", "/OrderService.SetOrderItemStatus", data,
            (data, response) : void => {
                let order = dataToModel<Order>(Order, data);

                onSuccess(order, response);
            },
            onError
        );
    }
    setOrderEndpointStatus (
        id: string,
        status: string,
        orderEndpointName: string,
        onSuccess: (order: Order, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Context: {
                OrderEndpointName: orderEndpointName,
                Status: status
            }
        };

        this.sendToId(id, "POST", "/OrderService.SetOrderEndpointStatus", data,
            (data, response) : void => {
                let order = dataToModel<Order>(Order, data);

                onSuccess(order, response);
            },
            onError
        );
    }
}
