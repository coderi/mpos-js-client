import { Type } from "class-transformer";

import BaseModel from "./BaseModel";

import UserSessionProfile from "./UserSessionProfile";

export default class UserSession extends BaseModel {
    TokenId         : string;

    @Type(() => UserSessionProfile)
    User            : UserSessionProfile;
}
