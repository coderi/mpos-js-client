import BaseModel from "./BaseModel";
export default class Vat extends BaseModel {
    Category: number;
    Rate: number;
}
