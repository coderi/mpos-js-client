"use strict";
var axios_1 = require("axios");
var Localize_1 = require("./locales/Localize");
var MPos = (function () {
    function MPos() {
    }
    MPos.initialize = function (apiUrl) {
        axios_1.default.defaults.baseURL = apiUrl;
        axios_1.default.defaults.headers.common["Content-Type"] = "application/json; charset=utf-8";
        axios_1.default.defaults.headers.common["Accept"] = "application/json; charset=utf-8";
    };
    MPos.setAccessToken = function (accessToken) {
        axios_1.default.defaults.headers.common["X-Access-Token"] = accessToken;
    };
    MPos.removeAccessToken = function () {
        delete axios_1.default.defaults.headers.common["X-Access-Token"];
    };
    MPos.setLanguage = function (language) {
        axios_1.default.defaults.headers.common["Accept-Language"] = language;
        Localize_1.Localize.setLocale(language);
    };
    MPos.removeLanguage = function () {
        delete axios_1.default.defaults.headers.common["Accept-Language"];
    };
    return MPos;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MPos;
//# sourceMappingURL=mpos.js.map