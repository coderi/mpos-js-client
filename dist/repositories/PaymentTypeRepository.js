"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var PaymentType_1 = require("../models/PaymentType");
var PaymentTypeRepository = (function (_super) {
    __extends(PaymentTypeRepository, _super);
    function PaymentTypeRepository() {
        return _super.call(this, "/PaymentTypes", PaymentType_1.default) || this;
    }
    return PaymentTypeRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PaymentTypeRepository;
//# sourceMappingURL=PaymentTypeRepository.js.map