import BaseModel from "./BaseModel";
import CashTransfer from "./CashTransfer";
import DailySalesReportFiscalInfo from "./DailySalesReportFiscalInfo";
export default class DailySalesReport extends BaseModel {
    Id: string;
    FiscalName: string;
    Number: number;
    FiscalMemorySerialNumber: string;
    CurrencyLabel: string;
    FiscalInfo: DailySalesReportFiscalInfo;
    OpenDate: Date;
    CashTransfers: CashTransfer[];
}
