var expect = require("chai").expect;

var mPosLib = require("../../dist/index.js");

var ArticleCategory = mPosLib.ArticleCategory;

describe("ArticleCategory", function() {
    describe("isValid", function() {
        var value = new ArticleCategory();

        it("should be false by default", function() {
            expect(value.isValid()).to.be.false;
        });
        it("should validate Color", function() {
            value.CourseNumber  = 1;
            value.Description   = "";
            value.Label         = "AAA";

            value.Color = "";
            expect(value.isValid()).to.be.false;
            value.Color = "aaa";
            expect(value.isValid()).to.be.true; // '#' sign is not required!
            value.Color = "#aaa";
            expect(value.isValid()).to.be.true;
            value.Color = "#aaaaaa";
            expect(value.isValid()).to.be.true;
        });
        it("should validate CourseNumber", function() {
            value.Color         = "#aaaaaa";
            value.Description   = "";
            value.Label         = "AAA";

            value.CourseNumber = "";
            expect(value.isValid()).to.be.false;
            value.CourseNumber = 1;
            expect(value.isValid()).to.be.true;
        });
        it("should validate Description", function() {
            value.CourseNumber  = 1;
            value.Color         = "#aaaaaa";
            value.Label         = "AAA";

            value.Description = 1;
            expect(value.isValid()).to.be.false;
            value.Description = "";
            expect(value.isValid()).to.be.true;
        });
        it("should validate Label", function() {
            value.CourseNumber  = 1;
            value.Color         = "#aaaaaa";
            value.Description   = "";

            value.Label = 1;
            expect(value.isValid()).to.be.false;
            value.Label = "";
            expect(value.isValid()).to.be.false;
            value.Label = "AAAA";
            expect(value.isValid()).to.be.false;
            value.Label = "AAA";
            expect(value.isValid()).to.be.true;
        });
    });
});
