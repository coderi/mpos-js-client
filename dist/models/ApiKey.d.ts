import BaseModel from "./BaseModel";
export default class ApiKey extends BaseModel {
    Id: string;
    ClientId: string;
    ClientSecret: string;
    IsActive: boolean;
    Name: string;
    Description: string;
    CallbackUrl: string;
    HomepageUrl: string;
    UserName: string;
    DeviceName: string;
    Permissions: string[];
    CreatedAt: Date;
}
