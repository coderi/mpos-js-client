"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var VatCategory_1 = require("../models/VatCategory");
var VatCategoryRepository = (function (_super) {
    __extends(VatCategoryRepository, _super);
    function VatCategoryRepository() {
        return _super.call(this, "/VatCategories", VatCategory_1.default) || this;
    }
    return VatCategoryRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = VatCategoryRepository;
//# sourceMappingURL=VatCategoryRepository.js.map