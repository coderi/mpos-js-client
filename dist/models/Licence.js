"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var BaseModel_1 = require("./BaseModel");
var Licence = (function (_super) {
    __extends(Licence, _super);
    function Licence() {
        return _super.apply(this, arguments) || this;
    }
    return Licence;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], Licence.prototype, "CertificateId", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsArray()
], Licence.prototype, "DKPs", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsArray()
], Licence.prototype, "Modules", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], Licence.prototype, "MaxDevicesCount", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], Licence.prototype, "RemainingDays", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], Licence.prototype, "IsExpired", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsDate(),
    class_transformer_1.Type(function () { return Date; }),
    class_transformer_1.Transform(function (value) { return value.toISOString(); }, { toPlainOnly: true })
], Licence.prototype, "ActivationDate", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsDate(),
    class_transformer_1.Type(function () { return Date; }),
    class_transformer_1.Transform(function (value) { return value.toISOString(); }, { toPlainOnly: true })
], Licence.prototype, "ExpirationDate", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Licence;
//# sourceMappingURL=Licence.js.map