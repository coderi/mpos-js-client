import { IsString, IsNumber, IsNotEmpty, IsHexColor, IsDefined, MaxLength } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

export default class ArticleCategory extends BaseModel {
    @IsString()
    @IsHexColor()
    Color       : string;

    @IsNumber()
    CourseNumber : number;

    @IsString()
    @IsDefined()
    Description : string;

    @IsString()
    @IsNotEmpty()
    @MaxLength(3, {
        message: t("error.validation.maxLength")
    })
    Label       : string;
}
