import { Type } from "class-transformer";

import { IsDefined, IsString, IsNumber, IsArray, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import OrderItemEndpoint from "./OrderItemEndpoint";
import OrderItemPluInfo from "./OrderItemPluInfo";
import Quantity from "./Quantity";

export default class OrderItem extends BaseModel {
    @IsString()
    Comment     : string;

    @IsNumber()
    Id          : number;

    @IsDefined()
    @IsArray()
    @Type(() => OrderItemEndpoint)
    Endpoints   : OrderItemEndpoint[];

    @IsDefined()
    @Type(() => OrderItemPluInfo)
    Plu         : OrderItemPluInfo;

    @ValidateNested()
    @IsDefined()
    @Type(() => Quantity)
    Quantity    : Quantity;
}
