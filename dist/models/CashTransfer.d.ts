import Money from "./Money";
import UserInfo from "./UserInfo";
import BaseModel from "./BaseModel";
export default class CashTransfer extends BaseModel {
    Date: Date;
    Amount: Money;
    Employee: UserInfo;
}
