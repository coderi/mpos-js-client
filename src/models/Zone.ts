import { Type } from "class-transformer";

import { IsDefined, IsString, IsArray, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import ZoneItem from "./ZoneItem";

export default class Zone extends BaseModel {
    @IsDefined()
    @IsString()
    Name               : string;

    @IsDefined()
    @IsString()
    DefaultFiscalName  : string;

    @ValidateNested()
    @IsDefined()
    @IsArray()
    @Type(() => ZoneItem)
    Items              : ZoneItem[];
}
