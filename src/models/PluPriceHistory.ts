import { Type, Transform } from "class-transformer";

import { IsDefined, IsDate, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import Money from "./Money";

export default class PluPriceHistory extends BaseModel {
    @IsDefined()
    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    Date        : Date;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    RetailPrice : Money;
}
