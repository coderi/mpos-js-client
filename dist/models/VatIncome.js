"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var Localize_1 = require("../locales/Localize");
var BaseModel_1 = require("./BaseModel");
var Money_1 = require("./Money");
var VatIncome = (function (_super) {
    __extends(VatIncome, _super);
    function VatIncome() {
        return _super.apply(this, arguments) || this;
    }
    return VatIncome;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber(),
    class_validator_1.Min(1, {
        message: Localize_1.default("error.validation.min")
    }),
    class_validator_1.Max(5, {
        message: Localize_1.default("error.validation.max")
    })
], VatIncome.prototype, "VatCategory", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber(),
    class_validator_1.Min(0, {
        message: Localize_1.default("error.validation.min")
    }),
    class_validator_1.Max(100, {
        message: Localize_1.default("error.validation.max")
    })
], VatIncome.prototype, "VatRate", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], VatIncome.prototype, "TotalAmount", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], VatIncome.prototype, "VatAmount", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = VatIncome;
//# sourceMappingURL=VatIncome.js.map