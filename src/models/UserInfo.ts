import { IsDefined, IsNotEmpty, IsString } from "class-validator";

import BaseModel from "./BaseModel";

export default class UserInfo extends BaseModel {
    @IsString()
    @IsNotEmpty()
    Name           : string;

    @IsString()
    @IsNotEmpty()
    UserName       : string;
}
