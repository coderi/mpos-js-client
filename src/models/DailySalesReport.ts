import { Type, Transform } from "class-transformer";

import { IsString, IsNumber, IsNotEmpty, IsDefined, IsDate, IsArray, Length } from "class-validator";

import BaseModel from "./BaseModel";

import CashTransfer from "./CashTransfer";
import DailySalesReportFiscalInfo from "./DailySalesReportFiscalInfo";

export default class DailySalesReport extends BaseModel {
    @IsString()
    Id                          : string;

    @IsString()
    @IsNotEmpty()
    FiscalName                  : string;

    @IsDefined()
    @IsNumber()
    Number                      : number;

    @IsDefined()
    @IsString()
    FiscalMemorySerialNumber    : string;

    @IsDefined()
    @IsString()
    @Length(3)
    CurrencyLabel               : string;

    @IsDefined()
    @Type(() => DailySalesReportFiscalInfo)
    FiscalInfo                 : DailySalesReportFiscalInfo;

    @IsDefined()
    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    OpenDate                    : Date;

    @IsDefined()
    @IsArray()
    @Type(() => CashTransfer)
    CashTransfers               : CashTransfer[];
}
