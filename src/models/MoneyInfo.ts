import { IsDefined, IsString, IsNumber, Length, Min } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import Money from "./Money";

var _ = require("lodash")

export default class MoneyInfo extends BaseModel {
    @IsDefined()
    @IsString()
    @Length(3)
    CurrencyLabel           : string;

    @IsDefined()
    @IsString()
    @Length(3)
    DomesticCurrencyLabel   : string;

    @IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    ExchangeRate            : number;

    @IsDefined()
    @IsNumber()
    Amount                  : number = 0;

    toDomestic () : MoneyInfo {
        let result = new MoneyInfo();
        let amount = _.round(this.Amount, 2) / _.round(this.ExchangeRate, 4);

        result.Amount = _.round(amount, 2);
        result.CurrencyLabel = this.DomesticCurrencyLabel;
        result.DomesticCurrencyLabel = this.DomesticCurrencyLabel;
        result.ExchangeRate = 1;

        return result;
    }

    toDomesticMoney () : Money {
        let domesticMoneyInfo = this.toDomestic();
        let result = new Money();

        result.Amount = domesticMoneyInfo.Amount;
        result.CurrencyLabel = domesticMoneyInfo.CurrencyLabel;

        return result;
    }
}
