import { Type, Transform } from "class-transformer";

import { IsDefined, IsString, IsNumber, IsNotEmpty, IsArray, IsBoolean, IsIn, Length, ValidateNested } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import Vat from "./Vat";

export default class TicketItemPlu extends BaseModel {
    @IsString()
    @Length(3, null, {
        message: t("error.validation.length")
    })
    ArticleCategoryLabel   : string;

    @IsDefined()
    @IsNumber()
    Code                   : number;

    @IsString()
    Description            : string;

    @IsDefined()
    @IsBoolean()
    IsDiscountAllowed      : boolean;

    @IsDefined()
    @IsBoolean()
    IsPriceFixed           : boolean;

    @IsDefined()
    @IsBoolean()
    IsSplittable           : boolean;

    @IsString()
    @IsNotEmpty()
    Name                   : string;

    @IsDefined()
    @IsArray()
    OrderEndpointNames     : string[];

    @IsDefined()
    @IsString()
    StockName              : string;

    @IsDefined()
    @IsString()
    @IsIn(["Unknown", "Recipe", "StockItem", "Credit", "Discount", "Service"])
    Type                   : string;

    @IsNumber()
    CourseNumber           : number;

    @ValidateNested()
    @IsDefined()
    @Type(() => Vat)
    VatInfo                : Vat;
}
