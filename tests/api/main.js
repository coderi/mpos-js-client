var DEBUG = false;

/*
 *  Dependencies
 */
var MPosLib = require("../../dist/index"); // In production: require("mpos-lib")
var async   = require("async");

var MPos = MPosLib.default;

/*
 *  Config
 */
var serverUrl = "http://84.16.39.69:3000";

/*
 *  Initialize API provider - Needs to be set only ONCE! (static, global)
 */
MPos.initialize(serverUrl);

/*
 *  Default Error Callback
 */
function createErrorCallback (callback) {
    return function (error, response) {
        console.log("Error: '" + error.Message + "'");

        if (DEBUG) console.log(response);

        callback();
    };
}

/*
 *  Repositories
 */
var authRepository      = new MPosLib.AuthRepository();
var fiscalRepository    = new MPosLib.FiscalRepository();
var licenceRepository   = new MPosLib.LicenceRepository();
var orderRepository     = new MPosLib.OrderRepository();
var ticketRepository    = new MPosLib.TicketRepository();
var userRepository      = new MPosLib.UserRepository();

var readRepositories = [
    new MPosLib.ApiKeyRepository(),
    new MPosLib.ArticleCategoryRepository(),
    new MPosLib.CompanyRepository(),
    new MPosLib.CustomerRepository(),
    new MPosLib.CurrencyRepository(),
    new MPosLib.DailySalesReportRepository(),
    new MPosLib.DeviceRepository(),
    new MPosLib.FiscalRepository(),
    new MPosLib.OrderRepository(),
    new MPosLib.OrderEndpointRepository(),
    new MPosLib.PaymentTypeRepository(),
    new MPosLib.PluRepository(),
    new MPosLib.RoleRepository(),
    new MPosLib.StockRepository(),
    new MPosLib.StockTakingRepository(),
    new MPosLib.TicketRepository(),
    new MPosLib.UserRepository(),
    new MPosLib.VatRepository(),
    new MPosLib.ZoneRepository()
];

/*
 *  ReadRepositories Test Case
 */
var testReadRepositories = [];

readRepositories.forEach(function (repository) {
    testReadRepositories.push(function (repositoryCallback) {
        // ReadRepository
        console.log("Test: " + repository.constructor.name);
        async.series(
            [
                // FindAll
                function (callback) {
                    process.stdout.write("\t .findAll ... ");
                    repository.findAll(
                        function (collection, response) {
                            console.log("OK");

                            if (DEBUG) console.log(response);

                            callback();
                        },
                        createErrorCallback(callback)
                    );
                }
            ],
            function (err, results) {
                repositoryCallback();
            }
        );
    });
});

/*
 *  TicketRepository Test Case
 */
var testTicketRepository = [
    // Create Ticket
    function (callback) {
        var newTicket = new Ticket();

        /*
        newTicket.Name = "O1";
        newTicket.PurchaseType = "WalkIn";
        newTicket.OriginDeviceName = "PDA1";
        newTicket.CurrencyLabel = "EUR";
        newTicket.DiscountRate = 0;

        newTicket.Employee = new User();
        newTicket.Employee.Name = "Bez vymazania polozky";
        newTicket.Employee.UserName = "5";

        newTicket.Items = [];
        newTicket.Payments = [];
        */

        process.stdout.write("\t .create ... ");
        ticketRepository.create(
            newTicket,
            function (model, response) {
                console.log("OK");

                if (DEBUG) console.log(response);

                callback();
            },
            createErrorCallback(callback)
        );
        callback();
    }
];

/*
 *  SpecialRepositories Test Case
 */
var testSpecialRepositories = [
    // TicketRepository
    function (callback) {
        console.log("Test: TicketRepository");
        async.series(
            testTicketRepository,
            function (err, results) {
                callback();
            }
        );
    }
];

/*
 *  Run Tests
 */
async.series(
    [
        // login
        function(callback) {
            process.stdout.write("AuthRepository.login ... ");
            authRepository.login (
                "999",          // Username
                "",             // Password
                "BackOffice",   // DeviceName
                function (session, response) {
                    console.log("OK");

                    if (DEBUG) console.log(response);

                    MPos.setAccessToken(session.TokenId);
                    callback();
                },
                createErrorCallback(callback)
            );
        },
        // loadCurrentUser
        function(callback) {
            process.stdout.write("AuthRepository.loadCurrentUser ... ");
            authRepository.loadCurrentUser (
                function (user, response) {
                    console.log("OK");

                    if (DEBUG) console.log(response);

                    callback();
                },
                createErrorCallback(callback)
            );
        },

        // Read Repositories
        function (callback) {
            // ReadRepositories
            async.series(
                testReadRepositories,
                function(err, results) {
                    callback();
                }
            );
        },
/*
        // Special Repositories
        function (callback) {
            async.series(
                testSpecialRepositories,
                function(err, results) {
                    callback();
                }
            );
        },
*/

        // logout
        function(callback) {
            process.stdout.write("AuthRepository.logout ... ");
            authRepository.logout (
                function (response) {
                    console.log("OK");

                    if (DEBUG) console.log(response);

                    MPos.removeAccessToken();
                    callback();
                },
                createErrorCallback(callback)
            );
        }
    ],
    function (err, results) {
        console.log("Bye");
    }
);
