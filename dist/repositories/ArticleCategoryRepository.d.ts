import { CRUDOdataRepository } from "./BaseRepository";
import ArticleCategory from "../models/ArticleCategory";
export default class ArticleCategoryRepository extends CRUDOdataRepository<ArticleCategory, string> {
    constructor();
}
