import BaseModel from "./BaseModel";
export default class BankAccount extends BaseModel {
    BankCountry: string;
    BankName: string;
    IBAN: string;
    Number: string;
    SWIFT: string;
}
