import BaseModel from "./BaseModel";
import OrderItemEndpoint from "./OrderItemEndpoint";
import OrderItemPluInfo from "./OrderItemPluInfo";
import Quantity from "./Quantity";
export default class OrderItem extends BaseModel {
    Comment: string;
    Id: number;
    Endpoints: OrderItemEndpoint[];
    Plu: OrderItemPluInfo;
    Quantity: Quantity;
}
