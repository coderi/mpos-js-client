import { CRUDOdataRepository } from "./BaseRepository";

import StockTaking from "../models/StockTaking";

export default class StockTakingRepository extends CRUDOdataRepository<StockTaking, string> { // Id
    constructor() {
        super("/StockTakings", StockTaking);
    }
}
