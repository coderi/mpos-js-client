import { Type, Transform } from "class-transformer";

import { IsDefined, IsString, IsNumber, IsNotEmpty, Length, ValidateNested } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import Money from "./Money";
import Vat from "./Vat";

export default class StockTakingItemPlu extends BaseModel {
    @IsString()
    @IsDefined()
    @Length(3, null, {
        message: t("error.validation.length")
    })
    ArticleCategoryLabel   : string;

    @IsDefined()
    @IsNumber()
    Code                   : number;

    @IsString()
    @IsNotEmpty()
    Name                   : string;

    @IsString()
    @IsNotEmpty()
    StockName              : string;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    UnitRetailPrice        : Money;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    UnitPurchasePrice      : Money;

    @ValidateNested()
    @Type(() => Vat)
    Vat                    : Vat;
}
