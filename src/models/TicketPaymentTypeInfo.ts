import { IsDefined, IsString, IsNumber, IsNotEmpty, IsBoolean, Min, Max } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

export default class TicketPaymentTypeInfo extends BaseModel {
    @IsDefined()
    @IsNumber()
    @Min(1, {
        message: t("error.validation.min")
    })
    @Max(10, {
        message: t("error.validation.max")
    })
    Number          : number;

    @IsString()
    @IsNotEmpty()
    Description     : string;

    @IsDefined()
    @IsBoolean()
    IsChangeable    : boolean;
}
