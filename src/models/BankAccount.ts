import { IsDefined, IsString } from "class-validator";

import BaseModel from "./BaseModel";

export default class BankAccount extends BaseModel {
    @IsDefined()
    @IsString()
    BankCountry : string;

    @IsDefined()
    @IsString()
    BankName    : string;

    @IsDefined()
    @IsString()
    IBAN        : string;

    @IsDefined()
    @IsString()
    Number      : string;

    @IsDefined()
    @IsString()
    SWIFT       : string;
}
