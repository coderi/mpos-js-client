import { Type } from "class-transformer";

import { IsDefined, IsBoolean, IsNumber, IsNotEmpty, IsString, IsArray, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import KeyValue from "./KeyValue";

export default class OrderEndpoint extends BaseModel {
    @IsDefined()
    @IsBoolean()
    IsActive   : boolean;

    @IsString()
    @IsNotEmpty()
    Name       : string;

    @IsDefined()
    @IsString()
    Address    : string;

    @IsString()
    @IsNotEmpty()
    Type       : string;

    @ValidateNested()
    @IsDefined()
    @IsArray()
    @Type(() => KeyValue)
    Settings   : KeyValue[];
}
