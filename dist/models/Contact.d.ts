import BaseModel from "./BaseModel";
export default class Contact extends BaseModel {
    Fax: string;
    Phone: string;
    Mobile: string;
    Email: string;
}
