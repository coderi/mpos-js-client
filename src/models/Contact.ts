import { IsDefined, IsString } from "class-validator";

import BaseModel from "./BaseModel";

export default class Contact extends BaseModel {
    @IsDefined()
    @IsString()
    Fax     : string;

	@IsDefined()
    @IsString()
    Phone   : string;

	@IsDefined()
    @IsString()
    Mobile  : string;

	@IsDefined()
    @IsString()
    Email   : string;
}
