import { ROdataRepository } from "./BaseRepository";
import { ExceptionCallback } from "../models/Exception";
import Order from "../models/Order";
export default class OrderRepository extends ROdataRepository<Order, string> {
    constructor();
    processOrders(ticketIds: string[], onSuccess: (orders: Order[], response: any) => void, onError: ExceptionCallback): void;
    setOrderStatus(id: string, status: string, onSuccess: (order: Order, response: any) => void, onError: ExceptionCallback): void;
    setOrderItemStatus(id: string, status: string, orderItemId: number, onSuccess: (order: Order, response: any) => void, onError: ExceptionCallback): void;
    setOrderEndpointStatus(id: string, status: string, orderEndpointName: string, onSuccess: (order: Order, response: any) => void, onError: ExceptionCallback): void;
}
