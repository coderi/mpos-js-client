"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_validator_1 = require("class-validator");
var Localize_1 = require("../locales/Localize");
var VatCategory = (function () {
    function VatCategory() {
    }
    return VatCategory;
}());
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber(),
    class_validator_1.Min(1, {
        message: Localize_1.default("error.validation.min")
    }),
    class_validator_1.Max(5, {
        message: Localize_1.default("error.validation.max")
    })
], VatCategory.prototype, "Category", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber(),
    class_validator_1.Min(0, {
        message: Localize_1.default("error.validation.min")
    }),
    class_validator_1.Max(100, {
        message: Localize_1.default("error.validation.max")
    })
], VatCategory.prototype, "Rate", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = VatCategory;
//# sourceMappingURL=VatCategory.js.map