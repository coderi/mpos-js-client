import { Type, Transform } from "class-transformer";

import { IsDefined, IsNotEmpty, IsString, ValidateNested } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import Address from "./Address";
import Contact from "./Contact";

export default class TicketCustomerInfo extends BaseModel {
    @IsString()
    CardId         : string;

    @IsString()
    CRN            : string;

    @IsString()
    @IsNotEmpty()
    Name           : string;

    @IsString()
    VatId          : string;

    @IsString()
    TaxId          : string;

    @ValidateNested()
    @IsDefined()
    @Type(() => Address)
    Address        : Address;

    @ValidateNested()
    @IsDefined()
    @Type(() => Contact)
    Contact        : Contact;
}
