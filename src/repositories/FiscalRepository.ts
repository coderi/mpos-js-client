import { plainToClass, classToPlain } from "class-transformer";

import { CRUDOdataRepository, dataToModel, dataToCollection } from "./BaseRepository";

import Exception, { ExceptionCallback } from "../models/Exception";

import Fiscal from "../models/Fiscal";
import CashTransfer from "../models/CashTransfer";
import DailySalesReport from "../models/DailySalesReport";

export default class FiscalRepository extends CRUDOdataRepository<Fiscal, string> { // Name
    constructor () {
        super("/Fiscals", Fiscal);
    }

    openDrawer (
        id: string,
        onSuccess: (response: any) => void,
        onError: ExceptionCallback
    ) : void {
        this.sendToId(id, "POST", "/FiscalService.OpenDrawer", {},
            (data, response) : void => {
                onSuccess(response);
            },
            onError
        );
    }
    createCashTransfer (
        id: string,
        cashTranfser: CashTransfer,
        onSuccess: (dailySalesReport: DailySalesReport, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            CashTransfer: classToPlain(cashTranfser)
        };

        this.sendToId(id, "POST", "/FiscalService.CashTransfer", data,
            (data, response) : void => {
                let dsr = dataToModel<DailySalesReport>(DailySalesReport, data);

                onSuccess(dsr, response);
            },
            onError
        );
    }
    printRecordCopy (
        id: string,
        dailySalesReportNumber: number,
        dailySalesReportDate : string,
        ticketNumber: number,
        onSuccess: (response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Context: {
                DailySalesReportNumber: dailySalesReportNumber,
                DailySalesReportDate:   dailySalesReportDate,
                TicketNumber:           ticketNumber
            }
        };

        this.sendToId(id, "POST", "/FiscalService.PrintRecordCopy", data,
            (data, response) : void => {
                onSuccess(response);
            },
            onError
        );
    }
    printLastRecordCopy (
        id: string,
        onSuccess: (response: any) => void,
        onError: ExceptionCallback
    ) : void {
        this.sendToId(id, "POST", "/FiscalService.PrintLastRecordCopy", {},
            (data, response) : void => {
                onSuccess(response);
            },
            onError
        );
    }
    printOverviewSalesReport (
        id: string,
        onSuccess: (response: any) => void,
        onError: ExceptionCallback
    ) : void {
        this.sendToId(id, "POST", "/FiscalService.PrintOverviewSalesReport", {},
            (data, response) : void => {
                onSuccess(response);
            },
            onError
        );
    }
    printDailySalesReport (
        id: string,
        onSuccess: (dailySalesReport: DailySalesReport, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        this.sendToId(id, "POST", "/FiscalService.DoDailySalesReport", {},
            (data, response) : void => {
                let dsr = dataToModel<DailySalesReport>(DailySalesReport, data);

                onSuccess(dsr, response);
            },
            onError
        );
    }
    printDailySalesReportCopy (
        id: string,
        dailySalesReportNumber: number,
        dailySalesReportDate : string,
        onSuccess: (response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Context: {
                DailySalesReportNumber: dailySalesReportNumber,
                DailySalesReportDate:   dailySalesReportDate
            }
        };

        this.sendToId(id, "POST", "/FiscalService.PrintDailySalesReportCopy", data,
            (data, response) : void => {
                onSuccess(response);
            },
            onError
        );
    }
    printLastDailySalesReportCopy (
        id: string,
        onSuccess: (response: any) => void,
        onError: ExceptionCallback
    ) : void {
        this.sendToId(id, "POST", "/FiscalService.PrintLastDailySalesReportCopy", {},
            (data, response) : void => {
                onSuccess(response);
            },
            onError
        );
    }
    printSummaryIntervalSalesReport (
        id: string,
        dsrDateFrom: string,
        dsrDateTo: string,
        dsrNumberFrom: string,
        dsrNumberTo: string,
        onSuccess: (response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Context: {
                DsrDateFrom:    dsrDateFrom,
                DsrDateTo:      dsrDateTo,
                DsrNumberFrom:  dsrNumberFrom,
                DsrNumberTo:    dsrNumberTo
            }
        };

        this.sendToId(id, "POST", "/FiscalService.PrintSummaryIntervalSalesReport", data,
            (data, response) : void => {
                onSuccess(response);
            },
            onError
        );
    }
    printDetailedIntervalSalesReport (
        id: string,
        dsrDateFrom: string,
        dsrDateTo: string,
        dsrNumberFrom: string,
        dsrNumberTo: string,
        onSuccess: (response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Context: {
                DsrDateFrom:    dsrDateFrom,
                DsrDateTo:      dsrDateTo,
                DsrNumberFrom:  dsrNumberFrom,
                DsrNumberTo:    dsrNumberTo
            }
        };

        this.sendToId(id, "POST", "/FiscalService.PrintDetailedIntervalSalesReport", data,
            (data, response) : void => {
                onSuccess(response);
            },
            onError
        );
    }
    printNonfiscalRecord (
        id: string,
        body: string,
        onSuccess: (response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Context: {
                RecordBody: body
            }
        };

        this.sendToId(id, "POST", "/FiscalService.PrintNonfiscalRecord", data,
            (data, response) : void => {
                onSuccess(response);
            },
            onError
        );
    }
}
