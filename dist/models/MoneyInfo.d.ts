import BaseModel from "./BaseModel";
import Money from "./Money";
export default class MoneyInfo extends BaseModel {
    CurrencyLabel: string;
    DomesticCurrencyLabel: string;
    ExchangeRate: number;
    Amount: number;
    toDomestic(): MoneyInfo;
    toDomesticMoney(): Money;
}
