"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var BaseModel_1 = require("./BaseModel");
var StockTakingItemPlu_1 = require("./StockTakingItemPlu");
var Quantity_1 = require("./Quantity");
var StockTakingItem = (function (_super) {
    __extends(StockTakingItem, _super);
    function StockTakingItem() {
        return _super.apply(this, arguments) || this;
    }
    return StockTakingItem;
}(BaseModel_1.default));
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Quantity_1.default; })
], StockTakingItem.prototype, "ActualQuantity", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Quantity_1.default; })
], StockTakingItem.prototype, "ExpectedQuantity", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return StockTakingItemPlu_1.default; })
], StockTakingItem.prototype, "Plu", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = StockTakingItem;
//# sourceMappingURL=StockTakingItem.js.map