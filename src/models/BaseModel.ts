import { validateSync } from "class-validator";

export default class BaseModel {
    isValid () : boolean {
        return (validateSync(this, { validationError: { value: false }, skipMissingProperties: true }).length === 0);
    }
}
