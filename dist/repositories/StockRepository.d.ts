import { CRUDOdataRepository } from "./BaseRepository";
import Stock from "../models/Stock";
export default class StockRepository extends CRUDOdataRepository<Stock, string> {
    constructor();
}
