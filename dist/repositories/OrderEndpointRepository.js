"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var OrderEndpoint_1 = require("../models/OrderEndpoint");
var OrderEndpointRepository = (function (_super) {
    __extends(OrderEndpointRepository, _super);
    function OrderEndpointRepository() {
        return _super.call(this, "/OrderEndpoints", OrderEndpoint_1.default) || this;
    }
    return OrderEndpointRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = OrderEndpointRepository;
//# sourceMappingURL=OrderEndpointRepository.js.map