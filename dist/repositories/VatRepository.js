"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var Vat_1 = require("../models/Vat");
var VatRepository = (function (_super) {
    __extends(VatRepository, _super);
    function VatRepository() {
        return _super.call(this, "/Vats", Vat_1.default) || this;
    }
    return VatRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = VatRepository;
//# sourceMappingURL=VatRepository.js.map