import { Type, Transform } from "class-transformer";

import { IsDefined, IsString, IsNumber, IsDate } from "class-validator";

import BaseModel from "./BaseModel";

export default class TicketCloseInfo extends BaseModel {
    @IsDefined()
    @IsString()
    DeviceName                  : string;

    @IsDefined()
    @IsString()
    FiscalName                  : string;

    @IsDefined()
    @IsNumber()
    Number                      : number;

    @IsDefined()
    @IsNumber()
    DailySalesReportNumber      : number;

    @IsDefined()
    @IsString()
    FiscalMemorySerialNumber    : string;

    @IsDefined()
    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    CloseDate                   : Date;
}
