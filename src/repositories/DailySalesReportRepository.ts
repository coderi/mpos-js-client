import { RUDOdataRepository } from "./BaseRepository";

import DailySalesReport from "../models/DailySalesReport";

export default class DailySalesReportRepository extends RUDOdataRepository<DailySalesReport, string> { // Id
    constructor() {
        super("/DailySalesReports", DailySalesReport);
    }
}
