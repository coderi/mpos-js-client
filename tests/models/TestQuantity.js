var expect = require("chai").expect;

var mPosLib = require("../../dist/index.js");

var Quantity = mPosLib.Quantity;

describe("Quantity", function() {
    describe("instantiation", function() {
        var qty = new Quantity();

        console.log(qty);
        console.log(qty.Amount);
        console.log(qty.Unit);

		it("should return object", function() {
            expect(qty).to.be.an('object');
        });
        it("should return Amount of type number", function() {
            expect(qty.Amount).to.be.an('number');
        });
        it("should have Amount set to 0", function() {
            expect(qty.Amount).to.equal(0);
        });
    });
    describe("rounding amount equal to zero", function() {
        var value = new Quantity();
        
        var result = value.getRoundedAmount();

        it("should result to 0", function() {
            expect(result).to.equal(0);
        });
    });
    describe("rounding amount with three decimal places", function() {
        var value = new Quantity();
        value.Amount = 1.234;

        var result = value.getRoundedAmount();

        it("should result to same value", function() {
            expect(result).to.equal(1.234);
        });
    });
    describe("rounding amount 1.1234", function() {
        var value = new Quantity();
        value.Amount = 1.1234;

        var result = value.getRoundedAmount();

        it("should result to value 1.123", function() {
            expect(result).to.equal(1.123);
        });
    });
    describe("rounding amount 1.1235", function() {
        var value = new Quantity();
        value.Amount = 1.1235;

        var result = value.getRoundedAmount();

        it("should result to value 1.124", function() {
            expect(result).to.equal(1.124);
        });
    });
});