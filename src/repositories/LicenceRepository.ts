import { ExceptionCallback } from "../models/Exception";

import Licence from "../models/Licence";

import { RestRepository, dataToModel } from "./BaseRepository";

export default class LicenceRepository extends RestRepository {
    constructor () {
        super("/licence");
    }

    getLicence (
        onSuccess: (licence: Licence, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        this.send("GET", "/", {},
            (data, response) => {
                let licence = dataToModel<Licence>(Licence, data);

                onSuccess(licence, response);
            },
            onError);
    }
    activateLicence (
        activationCode: string,
        onSuccess: (licence: Licence, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        this.send("POST", "/", activationCode,
            (data, response) => {
                let licence = dataToModel<Licence>(Licence, data);

                onSuccess(licence, response);
            },
            onError);
    }
    getLicenceRequestCode (
        maxDevicesCount: number,
        onSuccess: (activationCode: string, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        var url = "/requestcode" + (maxDevicesCount ? ("?MaxDevicesCount=" + maxDevicesCount) : "");
        this.send("GET", url, {},
            (data, response) => {
                onSuccess(data, response);
            },
            onError);
    }
}
