import BaseModel from "./BaseModel";
import KeyValue from "./KeyValue";
export default class OrderEndpoint extends BaseModel {
    IsActive: boolean;
    Name: string;
    Address: string;
    Type: string;
    Settings: KeyValue[];
}
