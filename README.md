# mPOS JavaScript Client library

## Usage

### Initialization

```javascript
var MPosLib = require("mpos-lib");

var MPos = MPosLib.default;

MPos.initialize("http://SERVER_ADDRESS:3000");

// Server error messages localization
MPos.setLanguage("sk-SK");
...
// After login
MPos.setAccessToken(accessToken);
...
// Use repositories freely
...
// After logout
MPos.removeAccessToken();
```

### Model usage

```javascript
var ExampleModel = MPosLib.ExampleModel;

var model = new ExampleModel();
...
model.property = "value";
...
```

### Repository usage

```javascript
var ExampleRepository = MPosLib.ExampleRepository;

var exampleRepository = new ExampleRepository();

// Find all
exampleRepository.findAll(
    // On success
    function (collection, response) {
        console.log(collection);
    },
    // On Error
    function (error, response) {
        console.err(error);
    }
);

// Find - filter/sort/paginate
exampleRepository.find(
    // Parameters
    {
        // Filter (optional)
        filter: "Description eq 'A'", // Filter query by OData V4 standard

        // Sort (optional)
        sort: {
            column: "",
            asc:    true, // true - Ascending, false - Descending
        },

        // Paginate (optional)
        paginate: {
            page:   1, // Page number, starting with 1
            limit:  10 // Items on page
        }
    },
    // On success
    function (collection, response) {
        console.log(collection);
    },
    // On error
    function (error, response) {
        console.err(error);
    }
);

// Find by Id
exampleRepository.findById(
    "123",
    // On success
    function (model) {
        console.log(model);
    },
    // On error
    function (error, response) {
        console.err(error);
    }
);

// Create
exampleRepository.create(
    model,
    // On success
    function (model, response) {
        console.log(model);
    },
    // On Error
    function (error, response) {
        console.err(error);
    }
);

// Update
exampleRepository.update(
    "123"
    model,
    // On success
    function (model, response) {
        console.log(model);
    },
    // On Error
    function (error, response) {
        console.err(error);
    }
);

// Remove
exampleRepository.delete(
    "123"
    // On success
    function (model, response) {
        console.log(model);
    },
    // On Error
    function (error, response) {
        console.err(error);
    }
);

```

## Build

1. Install dependencies:

    ```
    npm install
    ```

2. Install typescript:

    ```
    npm install -g typescript
    ```

3. Run build

    ```
    npm run build
    ```

* Run tests

    ```
    npm run test
    ```

* Run API tests

    ```
    npm run test-api
    ```

## Notes

- All http requests are made using [axios](https://github.com/mzabriskie/axios) library, so every ajax callback contains the axios response object as an argument.
