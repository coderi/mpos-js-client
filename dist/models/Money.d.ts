import BaseModel from "./BaseModel";
export default class Money extends BaseModel {
    Amount: number;
    CurrencyLabel: string;
    add(value: Money): Money;
    subtract(value: Money): Money;
    applyDiscount(normalizedDiscountRate: number): Money;
    multiply(value: Money): Money;
    divide(value: Money): Money;
    multiplyByNumber(value: number): Money;
    divideByNumber(value: number): Money;
    roundAmount(value: number): number;
}
