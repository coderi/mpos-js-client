import BaseModel from "./BaseModel";
import TicketLocation from "./TicketLocation";
export default class OrderTicketInfo extends BaseModel {
    Id: string;
    Name: string;
    Location: TicketLocation;
}
