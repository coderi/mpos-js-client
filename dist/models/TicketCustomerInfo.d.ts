import BaseModel from "./BaseModel";
import Address from "./Address";
import Contact from "./Contact";
export default class TicketCustomerInfo extends BaseModel {
    CardId: string;
    CRN: string;
    Name: string;
    VatId: string;
    TaxId: string;
    Address: Address;
    Contact: Contact;
}
