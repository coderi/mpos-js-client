import { Type } from "class-transformer";

import { IsDefined, IsString, IsBoolean, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import ZoneItemMeta from "./ZoneItemMeta";

export default class ZoneItem extends BaseModel {
    @IsDefined()
    @IsString()
    Name    : string;

    @IsDefined()
    @IsBoolean()
    IsTable : boolean;

    @ValidateNested()
    @IsDefined()
    @Type(() => ZoneItemMeta)
    Meta    : ZoneItemMeta;
}
