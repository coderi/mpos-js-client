import BaseModel from "./BaseModel";
export default class KeyValue extends BaseModel {
    Key: string;
    Value: string;
}
