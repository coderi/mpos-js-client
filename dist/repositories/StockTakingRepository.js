"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var StockTaking_1 = require("../models/StockTaking");
var StockTakingRepository = (function (_super) {
    __extends(StockTakingRepository, _super);
    function StockTakingRepository() {
        return _super.call(this, "/StockTakings", StockTaking_1.default) || this;
    }
    return StockTakingRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = StockTakingRepository;
//# sourceMappingURL=StockTakingRepository.js.map