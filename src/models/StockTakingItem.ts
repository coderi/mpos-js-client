import { Type } from "class-transformer";

import { IsDefined, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import StockTakingItemPlu from "./StockTakingItemPlu";
import Quantity from "./Quantity";

export default class StockTakingItem extends BaseModel {
    @ValidateNested()
    @IsDefined()
    @Type(() => Quantity)
    ActualQuantity   : Quantity;

    @ValidateNested()
    @IsDefined()
    @Type(() => Quantity)
    ExpectedQuantity : Quantity;

    @IsDefined()
    @Type(() => StockTakingItemPlu)
    Plu              : StockTakingItemPlu;
}
