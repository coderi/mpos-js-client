"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var BaseModel_1 = require("./BaseModel");
var UserDevice_1 = require("./UserDevice");
var UserCreateUpdateContext = (function (_super) {
    __extends(UserCreateUpdateContext, _super);
    function UserCreateUpdateContext() {
        return _super.apply(this, arguments) || this;
    }
    return UserCreateUpdateContext;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], UserCreateUpdateContext.prototype, "IsActive", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], UserCreateUpdateContext.prototype, "Name", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], UserCreateUpdateContext.prototype, "UserName", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], UserCreateUpdateContext.prototype, "Password", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], UserCreateUpdateContext.prototype, "ConfirmPassword", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_validator_1.IsArray(),
    class_transformer_1.Type(function () { return UserDevice_1.default; })
], UserCreateUpdateContext.prototype, "Devices", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = UserCreateUpdateContext;
//# sourceMappingURL=UserCreateUpdateContext.js.map