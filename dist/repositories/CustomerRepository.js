"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var Customer_1 = require("../models/Customer");
var CustomerRepository = (function (_super) {
    __extends(CustomerRepository, _super);
    function CustomerRepository() {
        return _super.call(this, "/Customers", Customer_1.default) || this;
    }
    return CustomerRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = CustomerRepository;
//# sourceMappingURL=CustomerRepository.js.map