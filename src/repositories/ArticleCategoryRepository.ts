import { CRUDOdataRepository } from "./BaseRepository";

import ArticleCategory from "../models/ArticleCategory";

export default class ArticleCategoryRepository extends CRUDOdataRepository<ArticleCategory, string> { // Label
    constructor() {
        super("/ArticleCategories", ArticleCategory);
    }
}
