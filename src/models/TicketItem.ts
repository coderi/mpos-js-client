import { Type } from "class-transformer";

import { IsDefined, IsString, IsNumber, Max, Min, ValidateNested } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import TicketItemPlu from "./TicketItemPlu";
import Money from "./Money";
import Quantity from "./Quantity";

export default class TicketItem extends BaseModel {
    @IsNumber()
    Id              : number;

    @IsDefined()
    @IsString()
    Comment         : string;

    @IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    @Max(100, {
        message: t("error.validation.max")
    })
    DiscountRate    : number = 0;

    @IsDefined()
    @Type(() => TicketItemPlu)
    Plu             : TicketItemPlu;

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    UnitPriceBrutto : Money;

    @ValidateNested()
    @IsDefined()
    @Type(() => Quantity)
    Quantity        : Quantity;

    getNormalizedDiscountRate () : number {
        return this.DiscountRate / 100;
    }

    getNettoUnitPrice () : Money {
        return this.UnitPriceBrutto.applyDiscount(this.getNormalizedDiscountRate());
    }

    getTotalBruttoPrice () : Money {
        return this.UnitPriceBrutto.multiplyByNumber(this.Quantity.getRoundedAmount());
    }

    getTotalNettoPrice () : Money {
        return this.getNettoUnitPrice().multiplyByNumber(this.Quantity.getRoundedAmount());
    }

    getDiscountAmount () : Money {
        return this.getTotalBruttoPrice().subtract(this.getTotalNettoPrice());
    }
}
