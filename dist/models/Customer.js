"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var Localize_1 = require("../locales/Localize");
var BaseModel_1 = require("./BaseModel");
var Address_1 = require("./Address");
var Contact_1 = require("./Contact");
var Money_1 = require("./Money");
var Customer = (function (_super) {
    __extends(Customer, _super);
    function Customer() {
        return _super.apply(this, arguments) || this;
    }
    return Customer;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString()
], Customer.prototype, "CardId", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], Customer.prototype, "CreditRate", void 0);
__decorate([
    class_validator_1.IsString()
], Customer.prototype, "CRN", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber(),
    class_validator_1.Min(0, {
        message: Localize_1.default("error.validation.min")
    }),
    class_validator_1.Max(100, {
        message: Localize_1.default("error.validation.max")
    })
], Customer.prototype, "DiscountRate", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], Customer.prototype, "IsActive", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty()
], Customer.prototype, "Name", void 0);
__decorate([
    class_validator_1.IsString()
], Customer.prototype, "VatId", void 0);
__decorate([
    class_validator_1.IsString()
], Customer.prototype, "TaxId", void 0);
__decorate([
    class_validator_1.IsDate(),
    class_transformer_1.Type(function () { return Date; }),
    class_transformer_1.Transform(function (value) { return value.toISOString(); }, { toPlainOnly: true })
], Customer.prototype, "ExpirationDate", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Address_1.default; })
], Customer.prototype, "Address", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Contact_1.default; })
], Customer.prototype, "Contact", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], Customer.prototype, "Credit", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Customer;
//# sourceMappingURL=Customer.js.map