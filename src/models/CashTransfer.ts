import { Type, Transform } from "class-transformer";

import { IsDefined, IsDate, IsArray } from "class-validator";

import Money from "./Money";
import UserInfo from "./UserInfo";

import BaseModel from "./BaseModel";

export default class CashTransfer extends BaseModel {
    @IsDefined()
    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    Date       : Date;

    @IsDefined()
    @Type(() => Money)
    Amount     : Money;

    @IsDefined()
    @IsArray()
    @Type(() => UserInfo)
    Employee   : UserInfo;
}
