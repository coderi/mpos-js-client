import { ExceptionCallback } from "../models/Exception";

import { RestRepository, dataToModel } from "./BaseRepository";

import { plainToClass } from "class-transformer";

import UserSession from "../models/UserSession";
import UserSessionProfile from "../models/UserSessionProfile";

export default class AuthRepository extends RestRepository {
    constructor () {
        super("/auth");
    }

    login (
        username:   string,
        password:   string,
        deviceName: string,
        onSuccess: (session: UserSession, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        this.send("POST", "", {
                UserName:   username,
                Password:   password,
                DeviceName: deviceName
            },
            (data, response) => {
                let session = dataToModel<UserSession>(UserSession, data);

                onSuccess(session, response);
            },
            onError);
    }
    logout (
        onSuccess: (response: any) => void,
        onError: ExceptionCallback
    ) : void {
        this.send("DELETE", "", {},
            (data, response) => {
                onSuccess(response);
            },
            onError);
    }
    loadCurrentUser (
        onSuccess: (user: UserSessionProfile, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        this.send("GET", "", {},
            (data, response) => {
                let user = dataToModel<UserSessionProfile>(UserSessionProfile, data);

                onSuccess(user, response);
            },
            onError);
    }
}
