import { classToPlain } from "class-transformer";

import { CRUDOdataRepository, dataToModel, dataToCollection } from "./BaseRepository";

import { ExceptionCallback } from "../models/Exception";

import UserInfo from "../models/UserInfo";
import Ticket from "../models/Ticket";

export default class TicketRepository extends CRUDOdataRepository<Ticket, string> { // Id
    constructor () {
        super ("/Tickets", Ticket);
    }

    closeTicket (
        id: string,
        employee: UserInfo,
        fiscalName: string,
        onSuccess: (ticket: Ticket, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Context: {
                Employee:   classToPlain(employee),
                FiscalName: fiscalName
            }
        };

        this.sendToId(id, "POST", "/TicketService.Close", data,
            (data, response) : void => {
                let ticket = dataToModel<Ticket>(Ticket, data);

                onSuccess(ticket, response);
            },
            onError
        );
    }
    printPreliminaryTicket (
        id: string,
        orderEndpointName: string,
        onSuccess: (ticket: Ticket, response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Context: {
                OrderEndpointName: orderEndpointName
            }
        };

        this.sendToId(id, "POST", "/TicketService.PrintPreliminaryTicket", data,
            (data, response) : void => {
                let ticket = dataToModel<Ticket>(Ticket, data);

                onSuccess(ticket, response);
            },
            onError
        );
    }
    updateTickets (
        tickets: Ticket[],
        action: string,
        onSuccess: (tickets: Ticket[], response: any) => void,
        onError: ExceptionCallback
    ) : void {
        let data = {
            Action:     action,
            Tickets:    classToPlain(tickets)
        };

        this.send("POST", "/TicketService.Update", data,
            (data, response) : void => {
                let tickets = dataToCollection<Ticket>(Ticket, data.value);

                onSuccess(tickets, response);
            },
            onError
        );
    }
}
