import { ExceptionCallback } from "../models/Exception";
import Licence from "../models/Licence";
import { RestRepository } from "./BaseRepository";
export default class LicenceRepository extends RestRepository {
    constructor();
    getLicence(onSuccess: (licence: Licence, response: any) => void, onError: ExceptionCallback): void;
    activateLicence(activationCode: string, onSuccess: (licence: Licence, response: any) => void, onError: ExceptionCallback): void;
    getLicenceRequestCode(maxDevicesCount: number, onSuccess: (activationCode: string, response: any) => void, onError: ExceptionCallback): void;
}
