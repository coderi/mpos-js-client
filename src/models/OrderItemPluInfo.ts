import { IsDefined, IsString, IsNumber } from "class-validator";

import BaseModel from "./BaseModel";

export default class OrderItemPluInfo extends BaseModel {
    @IsDefined()
    @IsNumber()
    Code        : number;

    @IsDefined()
    @IsString()
    Name        : string;

    @IsDefined()
    @IsString()
    StockName   : string;

    @IsNumber()
    CourseNumber : number;
}
