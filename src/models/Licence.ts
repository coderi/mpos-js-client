import { Type, Transform } from "class-transformer";

import { IsDefined, IsString, IsNumber, IsBoolean, IsDate, IsArray } from "class-validator";

import BaseModel from "./BaseModel";

export default class Licence extends BaseModel {
    @IsDefined()
    @IsNumber()
    CertificateId      : number;

    @IsDefined()
    @IsArray()
    DKPs               : string[];

    @IsDefined()
    @IsArray()
    Modules            : string[];

    @IsDefined()
    @IsNumber()
    MaxDevicesCount    : number;

    @IsDefined()
    @IsNumber()
    RemainingDays      : number;

    @IsDefined()
    @IsBoolean()
    IsExpired          : boolean;

    @IsDefined()
    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    ActivationDate     : Date;

    @IsDefined()
    @IsDate()
    @Type(() => Date)
    @Transform(value => value.toISOString(), { toPlainOnly: true })
    ExpirationDate     : Date;
}
