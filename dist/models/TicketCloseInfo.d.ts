import BaseModel from "./BaseModel";
export default class TicketCloseInfo extends BaseModel {
    DeviceName: string;
    FiscalName: string;
    Number: number;
    DailySalesReportNumber: number;
    FiscalMemorySerialNumber: string;
    CloseDate: Date;
}
