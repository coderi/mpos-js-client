import BaseModel from "./BaseModel";
import Money from "./Money";
import Vat from "./Vat";
export default class StockTakingItemPlu extends BaseModel {
    ArticleCategoryLabel: string;
    Code: number;
    Name: string;
    StockName: string;
    UnitRetailPrice: Money;
    UnitPurchasePrice: Money;
    Vat: Vat;
}
