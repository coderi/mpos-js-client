"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var class_transformer_1 = require("class-transformer");
var class_validator_1 = require("class-validator");
var Localize_1 = require("../locales/Localize");
var BaseModel_1 = require("./BaseModel");
var Quantity_1 = require("./Quantity");
var Money_1 = require("./Money");
var PluPriceHistory_1 = require("./PluPriceHistory");
var PluReceiptItem_1 = require("./PluReceiptItem");
var Plu = (function (_super) {
    __extends(Plu, _super);
    function Plu() {
        return _super.apply(this, arguments) || this;
    }
    return Plu;
}(BaseModel_1.default));
__decorate([
    class_validator_1.IsString(),
    class_validator_1.Length(3, null, {
        message: Localize_1.default("error.validation.length")
    })
], Plu.prototype, "ArticleCategoryLabel", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], Plu.prototype, "Code", void 0);
__decorate([
    class_validator_1.IsArray()
], Plu.prototype, "Codes", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsHexColor()
], Plu.prototype, "Color", void 0);
__decorate([
    class_validator_1.IsString()
], Plu.prototype, "Description", void 0);
__decorate([
    class_validator_1.IsString()
], Plu.prototype, "Id", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], Plu.prototype, "IsActive", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], Plu.prototype, "IsDiscountAllowed", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], Plu.prototype, "IsPriceFixed", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsBoolean()
], Plu.prototype, "IsSplittable", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty()
], Plu.prototype, "Name", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsArray()
], Plu.prototype, "OrderEndpointNames", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty()
], Plu.prototype, "StockName", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString(),
    class_validator_1.IsIn(["Recipe", "StockItem", "Credit", "Discount", "Service"])
], Plu.prototype, "Type", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsString(),
    class_validator_1.Length(3, null, {
        message: Localize_1.default("error.validation.length")
    })
], Plu.prototype, "Unit", void 0);
__decorate([
    class_validator_1.IsNumber()
], Plu.prototype, "CourseNumber", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsNumber()
], Plu.prototype, "VatCategory", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_transformer_1.Type(function () { return Quantity_1.default; })
], Plu.prototype, "MinStockQuantity", void 0);
__decorate([
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return PluPriceHistory_1.default; })
], Plu.prototype, "PriceHistory", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return PluReceiptItem_1.default; })
], Plu.prototype, "Receipt", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_validator_1.IsDefined(),
    class_transformer_1.Type(function () { return Money_1.default; })
], Plu.prototype, "RetailPrice", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_transformer_1.Type(function () { return Quantity_1.default; })
], Plu.prototype, "StockQuantity", void 0);
__decorate([
    class_validator_1.ValidateNested(),
    class_transformer_1.Type(function () { return Money_1.default; })
], Plu.prototype, "StockValue", void 0);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Plu;
//# sourceMappingURL=Plu.js.map