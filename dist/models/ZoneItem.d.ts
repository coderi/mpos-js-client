import BaseModel from "./BaseModel";
import ZoneItemMeta from "./ZoneItemMeta";
export default class ZoneItem extends BaseModel {
    Name: string;
    IsTable: boolean;
    Meta: ZoneItemMeta;
}
