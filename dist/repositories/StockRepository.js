"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseRepository_1 = require("./BaseRepository");
var Stock_1 = require("../models/Stock");
var StockRepository = (function (_super) {
    __extends(StockRepository, _super);
    function StockRepository() {
        return _super.call(this, "/Stocks", Stock_1.default) || this;
    }
    return StockRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = StockRepository;
//# sourceMappingURL=StockRepository.js.map