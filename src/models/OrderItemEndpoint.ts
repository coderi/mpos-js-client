import { Type } from "class-transformer";

import { IsDefined, IsString, IsArray, ValidateNested } from "class-validator";

import BaseModel from "./BaseModel";

import OrderItemEndpointStatusChange from "./OrderItemEndpointStatusChange";

export default class OrderItemEndpoint extends BaseModel {
    @IsDefined()
    @IsString()
    Name            : string;

    @ValidateNested()
    @IsDefined()
    @IsArray()
    @Type(() => OrderItemEndpointStatusChange)
    StatusChanges   : OrderItemEndpointStatusChange[];
}
