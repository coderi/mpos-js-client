import { RDOdataRepository } from "./BaseRepository";
import { ExceptionCallback } from "../models/Exception";
import User from "../models/User";
import UserCreateUpdateContext from "../models/UserCreateUpdateContext";
export default class UserRepository extends RDOdataRepository<User, string> {
    constructor();
    create(model: UserCreateUpdateContext, onSuccess: (model: User, response: any) => void, onError: ExceptionCallback): void;
    update(id: string, model: UserCreateUpdateContext, onSuccess: (model: User, response: any) => void, onError: ExceptionCallback): void;
}
