import { Type, Transform } from "class-transformer";

import { IsDefined, IsString, IsNumber, IsNotEmpty, IsArray, IsHexColor, IsBoolean, IsIn, Length, ValidateNested } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import Quantity from "./Quantity";
import Money from "./Money";
import Vat from "./Vat";
import PluPriceHistory from "./PluPriceHistory";
import PluReceiptItem from "./PluReceiptItem";

export default class Plu extends BaseModel {
    @IsString()
    @Length(3, null, {
        message: t("error.validation.length")
    })
    ArticleCategoryLabel   : string;

    @IsDefined()
    @IsNumber()
    Code                   : number;

    @IsArray()
    Codes                  : string[];

    @IsString()
    @IsHexColor()
    Color                  : string;

    @IsString()
    Description            : string;

    @IsString()
    Id                     : string;

    @IsDefined()
    @IsBoolean()
    IsActive               : boolean;

    @IsDefined()
    @IsBoolean()
    IsDiscountAllowed      : boolean;

    @IsDefined()
    @IsBoolean()
    IsPriceFixed           : boolean;

    @IsDefined()
    @IsBoolean()
    IsSplittable           : boolean;

    @IsString()
    @IsNotEmpty()
    Name                   : string;

    @IsDefined()
    @IsArray()
    OrderEndpointNames     : string[];

    @IsString()
    @IsNotEmpty()
    StockName              : string;

    @IsDefined()
    @IsString()
    @IsIn(["Recipe", "StockItem", "Credit", "Discount", "Service"])
    Type                   : string;

    @IsDefined()
    @IsString()
    @Length(3, null, {
        message: t("error.validation.length")
    })
    Unit                   : string;

    @IsNumber()
    CourseNumber           : number;

    @ValidateNested()
    @IsNumber()
    VatCategory            : number;

    @ValidateNested()
    @Type(() => Quantity)
    MinStockQuantity       : Quantity;

    @IsDefined()
    @Type(() => PluPriceHistory)
    PriceHistory           : PluPriceHistory[];

    @ValidateNested()
    @IsDefined()
    @Type(() => PluReceiptItem)
    Receipt                : PluReceiptItem[];

    @ValidateNested()
    @IsDefined()
    @Type(() => Money)
    RetailPrice            : Money;

    @ValidateNested()
    @Type(() => Quantity)
    StockQuantity          : Quantity;

    @ValidateNested()
    @Type(() => Money)
    StockValue             : Money;
}
