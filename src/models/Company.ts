import { Type } from "class-transformer";

import { IsDefined, IsString, IsBoolean, IsNumber, IsNotEmpty, IsIn, Max, Min, ValidateNested } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

import Address from "./Address";
import BankAccount from "./BankAccount";
import Contact from "./Contact";

export default class Company extends BaseModel {
    @IsString()
    Id              : string;

    @IsString()
    @IsNotEmpty()
    Name            : string;

    @IsDefined()
    @IsString()
    CRN             : string;

    @IsDefined()
    @IsString()
    VatId           : string;

    @IsDefined()
    @IsString()
    TaxId           : string;

    @IsDefined()
    @IsBoolean()
    TaxPayer        : boolean;

    @IsDefined()
    @IsString()
    Register        : string;

    @IsDefined()
    @IsNumber()
    DueDays         : number;

    @IsDefined()
    @IsString()
    @IsIn(["Unkonwn", "Supplier", "Purchaser"])
    Type            : string;

    @IsDefined()
    @IsBoolean()
    IsDefault       : boolean;

    @IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    @Max(100, {
        message: t("error.validation.max")
    })
    DiscountRate    : number;

    @IsDefined()
    @ValidateNested()
    @Type(() => Address)
    Address         : Address;

    @ValidateNested()
    @Type(() => Address)
    ShippingAddress : Address;

    @IsDefined()
    @ValidateNested()
    @Type(() => BankAccount)
    BankAccount     : BankAccount;

    @IsDefined()
    @ValidateNested()
    @Type(() => Contact)
    Contact         : Contact;
}
