import BaseModel from "./BaseModel";
export default class TicketLocation extends BaseModel {
    TableName: string;
    ZoneName: string;
}
