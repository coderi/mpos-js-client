import { IsDefined, IsString, IsNumber, Length } from "class-validator";

var _ = require("lodash")

import BaseModel from "./BaseModel";

export default class Money extends BaseModel {
    @IsDefined()
    @IsNumber()
    Amount          : number = 0;

    @IsDefined()
    @IsString()
    @Length(3)
    CurrencyLabel   : string;

    add (value: Money) : Money {
    	let result = new Money();
    	let amount = this.roundAmount(this.Amount + value.Amount);

    	result.Amount = amount;
    	result.CurrencyLabel = this.CurrencyLabel;

		return result;
    }

    subtract (value: Money) : Money {
    	let result = new Money();
    	let amount = this.roundAmount(this.Amount - value.Amount);

    	result.Amount = amount;
    	result.CurrencyLabel = this.CurrencyLabel;

		return result;
    }

    applyDiscount (normalizedDiscountRate: number) : Money {
    	let result = new Money();
        let delta = this.roundAmount(this.Amount * normalizedDiscountRate);
    	let amount = this.roundAmount(this.Amount - delta);

    	result.Amount = amount;
    	result.CurrencyLabel = this.CurrencyLabel;

		return result;
    }

    multiply (value: Money) : Money {
    	let result = new Money();
    	let amount = this.roundAmount(this.Amount * value.Amount);

    	result.Amount = amount;
    	result.CurrencyLabel = this.CurrencyLabel;

		return result;
    }

	divide (value: Money) : Money {
    	let result = new Money();
    	let amount = this.roundAmount(this.Amount / value.Amount);

    	result.Amount = amount;
    	result.CurrencyLabel = this.CurrencyLabel;

		return result;
    }

    multiplyByNumber (value: number) : Money {
        let result = new Money();
        let amount = this.roundAmount(this.Amount * value);

        result.Amount = amount;
        result.CurrencyLabel = this.CurrencyLabel;

        return result;
    }

    divideByNumber (value: number) : Money {
        let result = new Money();
        let amount = this.roundAmount(this.Amount / value);

        result.Amount = amount;
        result.CurrencyLabel = this.CurrencyLabel;

        return result;
    }

    roundAmount (value: number) : number {
        return _.round(value, 2);
	}
}
