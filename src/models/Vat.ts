import { IsDefined, IsNumber, Min, Max } from "class-validator";

import t from "../locales/Localize";

import BaseModel from "./BaseModel";

export default class Vat extends BaseModel {
    @IsDefined()
    @IsNumber()
    @Min(1, {
        message: t("error.validation.min")
    })
    @Max(5, {
        message: t("error.validation.max")
    })
    Category    : number = 0;

    @IsDefined()
    @IsNumber()
    @Min(0, {
        message: t("error.validation.min")
    })
    @Max(100, {
        message: t("error.validation.max")
    })
    Rate        : number = 0;
}
