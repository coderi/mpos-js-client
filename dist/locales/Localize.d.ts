export declare class Localize {
    static locale: string;
    static setLocale(locale: string): void;
}
export default function translate(key: string, params?: any[]): string;
