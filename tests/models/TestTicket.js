var expect = require("chai").expect;

var mPosLib = require("../../dist/index.js");

var Ticket = mPosLib.Ticket;
var TicketItem = mPosLib.TicketItem;
var Money = mPosLib.Money;
var Quantity = mPosLib.Quantity;
var Plu = mPosLib.Plu;

describe("Ticket", function() {
    describe("Ticket from calculations example #1", function() {
        var ticketItem = new TicketItem();
        ticketItem.DiscountRate = 5;
        ticketItem.UnitPriceBrutto = new Money();
        ticketItem.UnitPriceBrutto.Amount = 1.50;
        ticketItem.UnitPriceBrutto.CurrencyLabel = "EUR";
        ticketItem.Quantity = new Quantity();
        ticketItem.Quantity.Amount = 0.300;
        ticketItem.Plu = new Plu();
        ticketItem.Plu.IsDiscountAllowed = true;

        var ticket = new Ticket();
        ticket.DiscountRate = 0;
        ticket.CurrencyLabel = "EUR";
        ticket.Items = [
            ticketItem
        ];

        it("item should have total brutto price set to 0.45", function() {
            expect(ticketItem.getTotalBruttoPrice().Amount).to.equal(0.45);
        });
        it("item should have netto unit price set to 1.42", function() {
            expect(ticketItem.getNettoUnitPrice().Amount).to.equal(1.42);
        });
        it("item should have total netto price set to 0.43", function() {
            expect(ticketItem.getTotalNettoPrice().Amount).to.equal(0.43);
        });
        it("item should have discount amount set to 0.02", function() {
            expect(ticketItem.getDiscountAmount().Amount).to.equal(0.02);
        });
        it("ticket should have total brutto price set to 0.45", function() {
            expect(ticket.getTotalBruttoPrice().Amount).to.equal(0.45);
        });
        it("ticket should have total netto price set to 0.43", function() {
            expect(ticket.getTotalNettoPrice().Amount).to.equal(0.43);
        });
        it("should have set 'Discount Amount' to 0.02", function() {
            expect(ticket.getDiscountAmount().Amount).to.equal(0.02);
        });
    });

    describe("Ticket from calculations Example #2", function() {
        var ticketItem = new TicketItem();
        ticketItem.DiscountRate = 5;
        ticketItem.UnitPriceBrutto = new Money();
        ticketItem.UnitPriceBrutto.Amount = 2.90;
        ticketItem.UnitPriceBrutto.CurrencyLabel = "EUR";
        ticketItem.Quantity = new Quantity();
        ticketItem.Quantity.Amount = 1.000;
        ticketItem.Plu = new Plu();
        ticketItem.Plu.IsDiscountAllowed = true;

        var ticket = new Ticket();
        ticket.DiscountRate = 0;
        ticket.CurrencyLabel = "EUR";
        ticket.Items = [
            ticketItem
        ];

        it("should have set item 'Netto Unit Price' to 2.75", function() {
            expect(ticketItem.getNettoUnitPrice().Amount).to.equal(2.75);
        });
        it("should have set 'Total Brutto Price' to 2.90", function() {
            expect(ticket.getTotalBruttoPrice().Amount).to.equal(2.90);
        });
        it("should have set 'Total Netto Price' to 2.75", function() {
            expect(ticket.getTotalNettoPrice().Amount).to.equal(2.75);
        });
        it("should have set 'Discount Amount' to 0.15", function() {
            expect(ticket.getDiscountAmount().Amount).to.equal(0.15);
        });
    });

    describe("Ticket from calculations Example #3", function() {
        var ticketItem1 = new TicketItem();
        ticketItem1.DiscountRate = 50;
        ticketItem1.UnitPriceBrutto = new Money();
        ticketItem1.UnitPriceBrutto.Amount = 8.69;
        ticketItem1.UnitPriceBrutto.CurrencyLabel = "EUR";
        ticketItem1.Quantity = new Quantity();
        ticketItem1.Quantity.Amount = 1.000;
        ticketItem1.Plu = new Plu();
        ticketItem1.Plu.IsDiscountAllowed = true;

        var ticketItem2 = new TicketItem();
        ticketItem2.DiscountRate = 75;
        ticketItem2.UnitPriceBrutto = new Money();
        ticketItem2.UnitPriceBrutto.Amount = 8.69;
        ticketItem2.UnitPriceBrutto.CurrencyLabel = "EUR";
        ticketItem2.Quantity = new Quantity();
        ticketItem2.Quantity.Amount = 1.000;
        ticketItem2.Plu = new Plu();
        ticketItem2.Plu.IsDiscountAllowed = true;

        var ticket = new Ticket();
        ticket.DiscountRate = 0;
        ticket.CurrencyLabel = "EUR";
        ticket.Items = [
            ticketItem1,
            ticketItem2
        ];

        it("1st ticket item should have total brutto price equal to 8.69", function() {
            expect(ticketItem1.getTotalBruttoPrice().Amount).to.equal(8.69);
        });
        it("1st ticket item should have total netto price equal to 4.34", function() {
            expect(ticketItem1.getTotalNettoPrice().Amount).to.equal(4.34);
        });
        it("1st ticket item should have discount amount equal to 4.35", function() {
            expect(ticketItem1.getDiscountAmount().Amount).to.equal(4.35);
        });
        it("2nd ticket item should have total brutto price equal to 8.69", function() {
            expect(ticketItem2.getTotalBruttoPrice().Amount).to.equal(8.69);
        });
        it("2nd ticket item should have total netto price equal to 2.17", function() {
            expect(ticketItem2.getTotalNettoPrice().Amount).to.equal(2.17);
        });
        it("2nd ticket item should have discount amount equal to 6.52", function() {
            expect(ticketItem2.getDiscountAmount().Amount).to.equal(6.52);
        });
        it("ticket should have total brutto price set to 17.38", function() {
            expect(ticket.getTotalBruttoPrice().Amount).to.equal(17.38);
        });
        it("ticket should have total netto price set to 6.51", function() {
            expect(ticket.getTotalNettoPrice().Amount).to.equal(6.51);
        });
        it("ticket should have discount amount set to 10.87", function() {
            expect(ticket.getDiscountAmount().Amount).to.equal(10.87);
        });
    });
    
    describe("Ticket from calculations example #4", function() {
        var ticketItem1 = new TicketItem();
        ticketItem1.DiscountRate = 0;
        ticketItem1.UnitPriceBrutto = new Money();
        ticketItem1.UnitPriceBrutto.Amount = 8.69;
        ticketItem1.UnitPriceBrutto.CurrencyLabel = "EUR";
        ticketItem1.Quantity = new Quantity();
        ticketItem1.Quantity.Amount = 1.000;
        ticketItem1.Plu = new Plu();
        ticketItem1.Plu.IsDiscountAllowed = true;

        var ticketItem2 = new TicketItem();
        ticketItem2.DiscountRate = 50;
        ticketItem2.UnitPriceBrutto = new Money();
        ticketItem2.UnitPriceBrutto.Amount = 8.69;
        ticketItem2.UnitPriceBrutto.CurrencyLabel = "EUR";
        ticketItem2.Quantity = new Quantity();
        ticketItem2.Quantity.Amount = 1.000;
        ticketItem2.Plu = new Plu();
        ticketItem2.Plu.IsDiscountAllowed = true;

        var ticket = new Ticket();
        ticket.DiscountRate = 50;
        ticket.CurrencyLabel = "EUR";
        ticket.Items = [
            ticketItem1,
            ticketItem2
        ];

        it("1st ticket item should have total brutto price equal to 8.69", function() {
            expect(ticketItem1.getTotalBruttoPrice().Amount).to.equal(8.69);
        });
        it("1st ticket item should have total netto price equal to 8.69", function() {
            expect(ticketItem1.getTotalNettoPrice().Amount).to.equal(8.69);
        });
        it("1st ticket item should have discount amount equal to 0", function() {
            expect(ticketItem1.getDiscountAmount().Amount).to.equal(0);
        });
        it("2nd ticket item should have total brutto price equal to 8.69", function() {
            expect(ticketItem2.getTotalBruttoPrice().Amount).to.equal(8.69);
        });
        it("2nd ticket item should have total netto price equal to 4.34", function() {
            expect(ticketItem2.getTotalNettoPrice().Amount).to.equal(4.34);
        });
        it("2nd ticket item should have discount amount equal to 4.35", function() {
            expect(ticketItem2.getDiscountAmount().Amount).to.equal(4.35);
        });
        it("ticket should have total brutto price set to 17.38", function() {
            expect(ticket.getTotalBruttoPrice().Amount).to.equal(17.38);
        });
        it("ticket should have total netto price set to 6.51", function() {
            expect(ticket.getTotalNettoPrice().Amount).to.equal(6.51);
        });
        it("ticket should have discount amount set to 10.87", function() {
            expect(ticket.getDiscountAmount().Amount).to.equal(10.87);
        });
    });

    describe("Empty ticket", function() {
        var ticket = new Ticket();
        ticket.DiscountRate = 50;
        ticket.CurrencyLabel = "EUR";
        ticket.Items = [];

        it("should have total netto amount set to 0", function() {
            expect(ticket.getTotalNettoPrice().Amount).to.equal(0);
        });
        it("should have total brutto amount set to 0", function() {
            expect(ticket.getTotalBruttoPrice().Amount).to.equal(0);
        });
        if("should return truthy value from isEmpty() function", function() {
            expect(ticket.isEmpty).to.be.true;
        });
    });
});