import { CRUDOdataRepository } from "./BaseRepository";
import Vat from "../models/Vat";
export default class VatRepository extends CRUDOdataRepository<Vat, number> {
    constructor();
}
