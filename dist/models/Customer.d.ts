import BaseModel from "./BaseModel";
import Address from "./Address";
import Contact from "./Contact";
import Money from "./Money";
export default class Customer extends BaseModel {
    CardId: string;
    CreditRate: number;
    CRN: string;
    DiscountRate: number;
    IsActive: boolean;
    Name: string;
    VatId: string;
    TaxId: string;
    ExpirationDate: Date;
    Address: Address;
    Contact: Contact;
    Credit: Money;
}
