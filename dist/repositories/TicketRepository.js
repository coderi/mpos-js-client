"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var class_transformer_1 = require("class-transformer");
var BaseRepository_1 = require("./BaseRepository");
var Ticket_1 = require("../models/Ticket");
var TicketRepository = (function (_super) {
    __extends(TicketRepository, _super);
    function TicketRepository() {
        return _super.call(this, "/Tickets", Ticket_1.default) || this;
    }
    TicketRepository.prototype.closeTicket = function (id, employee, fiscalName, onSuccess, onError) {
        var data = {
            Context: {
                Employee: class_transformer_1.classToPlain(employee),
                FiscalName: fiscalName
            }
        };
        this.sendToId(id, "POST", "/TicketService.Close", data, function (data, response) {
            var ticket = BaseRepository_1.dataToModel(Ticket_1.default, data);
            onSuccess(ticket, response);
        }, onError);
    };
    TicketRepository.prototype.printPreliminaryTicket = function (id, orderEndpointName, onSuccess, onError) {
        var data = {
            Context: {
                OrderEndpointName: orderEndpointName
            }
        };
        this.sendToId(id, "POST", "/TicketService.PrintPreliminaryTicket", data, function (data, response) {
            var ticket = BaseRepository_1.dataToModel(Ticket_1.default, data);
            onSuccess(ticket, response);
        }, onError);
    };
    TicketRepository.prototype.updateTickets = function (tickets, action, onSuccess, onError) {
        var data = {
            Action: action,
            Tickets: class_transformer_1.classToPlain(tickets)
        };
        this.send("POST", "/TicketService.Update", data, function (data, response) {
            var tickets = BaseRepository_1.dataToCollection(Ticket_1.default, data.value);
            onSuccess(tickets, response);
        }, onError);
    };
    return TicketRepository;
}(BaseRepository_1.CRUDOdataRepository));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TicketRepository;
//# sourceMappingURL=TicketRepository.js.map