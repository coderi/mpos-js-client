import BaseModel from "./BaseModel";
import Coordinates from "./Coordinates";
export default class ZoneItemMeta extends BaseModel {
    Height: number;
    Width: number;
    Shape: string;
    Rotation: number;
    Color: string;
    Position: Coordinates;
}
